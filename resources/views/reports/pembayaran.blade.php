<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pembayaran</title>
    <style>
        @page { size: 121mm 315mm; margin: 0; padding: 0; }
        .page-break {
            page-break-after: always;
        }
        body {
            font-size: 10px;
        }
        .nm_tp {
            padding-left: <?php echo $paddingLeft[0] ?>;
            padding-top: 17mm;
        }
        .nm_tp_1 {
            padding-left: <?php echo $paddingLeft[0] ?>;
        }
        .thn_pajak {
            padding-left: <?php echo $paddingLeft[1] ?>;
        }
        .alamat {
            padding-left: <?php echo $paddingLeft[2] ?>;
        }
        .regional {
            padding-left: <?php echo $paddingLeft[1] ?>;
        }
        .nop {
            padding-left: <?php echo $paddingLeft[3] ?>;
        }
        .tgl_jatuh_tempo {
            padding-left: <?php echo $paddingLeft[3] ?>;
            padding-top: <?php echo $paddingTop[0] ?>;
        }
        .denda {
            padding-left: <?php echo $paddingLeft[4] ?>;
            padding-top: <?php echo $paddingTop[1] ?>;
        }
        .pembayaran {
            padding-top: <?php echo $paddingTop[2] ?>;
        }
        .pembayaran-tgl {
            padding-left: <?php echo $paddingLeft[2] ?>;
        }
        .pembayaran-total {
            padding-left: <?php echo $paddingLeft[3] ?>;
        }
    </style>
</head>
<body>
    @foreach ($pembayaran_sppt as $key => $row)
        @php
            $jatuh_tempo = $row[0]['tgl_jatuh_tempo_sppt'];
            $dateNow = \Carbon\Carbon::now();
            $diff = $jatuh_tempo->diffInMonths($dateNow);
        @endphp

        <p>
            <div class="nm_tp">{{ $row[0]['nm_tp'] }}</div>
            <div class="thn_pajak">{{ $row[0]['thn_pajak_sppt'] }}</div>
            <div class="alamat">{{ $row[0]['nm_wp_sppt'] }}</div>
            <div class="regional">{{ $row[0]['nm_kecamatan'] }}</div>
            <div class="regional">{{ $row[0]['kelurahan_wp_sppt'] }}</div>
            <div class="nop">{{ $row[0]['identitas'] }}</div>
            <div class="nop">{{ $row[0]['jml_sppt_yg_dibayar'] }}</div>
            <div class="tgl_jatuh_tempo">{{ $row[0]['tgl_jatuh_tempo_sppt'] }}</div>
            @if ($row[0]['denda_sppt'] > 0)
            <div class="denda">
                <div>2 persen x {{ $diff > 24 ? 24:$diff }} bulan x {{ number_format($row[0]['pbb_yg_harus_dibayar_sppt']) }}</div>
                <div>{{ number_format($row[0]['denda_sppt']) }}</div>
                <div>{{ number_format($row[0]['jml_sppt_yg_dibayar']) }}</div>
            </div>
            @endif
            <div class="pembayaran">
                <div class="pembayaran-tgl">{{ $row[0]['tgl_pembayaran_sppt'] }}</div>
                <div class="pembayaran-total">{{ number_format($row[0]['jml_sppt_yg_dibayar']) }}</div>
            </div>
            <div class="pembayaran">
                <div class="nm_tp_1">{{ $row[0]['nm_tp'] }}</div>
                <div class="thn_pajak">{{ $row[0]['thn_pajak_sppt'] }}</div>
                <div class="alamat">{{ $row[0]['nm_wp_sppt'] }}</div>
                <div class="regional">{{ $row[0]['nm_kecamatan'] }}</div>
                <div class="regional">{{ $row[0]['kelurahan_wp_sppt'] }}</div>
                <div class="nop">{{ $row[0]['identitas'] }}</div>
                <div class="nop">{{ number_format($row[0]['jml_sppt_yg_dibayar']) }}</div>
                <div class="pembayaran-tgl">{{ $row[0]['tgl_pembayaran_sppt'] }}</div>
                <div class="pembayaran-total">{{ number_format($row[0]['jml_sppt_yg_dibayar']) }}</div>
            </div>
        </p>
        @if (count($pembayaran_sppt) > 0 && $key < count($pembayaran_sppt)) 
            <div class="page-break"></div>
        @endif
    @endforeach
</body>
</html>