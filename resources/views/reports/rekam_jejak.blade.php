<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Rekam Jejak</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>
    <div class="container-fluid">
        <div class="text-center">
            <h5>PEMERINTAHAN KABUPATEN TAKALAR</h5>
            <h5>BPKD KABUPATEN TAKALAR</h5>
            <h5>MONITORING PEMBAYARAN WAJIB PAJAK</h5>
        </div>
        <hr>
        <table width="100%">
            <tr>
                <th>NOP</th>
                <td>:</td>
                <td>{{ $nop }}</td>
            </tr>
            <tr>
                <th>NAMA WP</th>
                <td>:</td>
                <td>{{ $wajibPajak[0]->nm_wp_sppt }}</td>
            </tr>
            <tr>
                <th>ALAMAT WP</th>
                <td>:</td>
                <td>{{ $wajibPajak[0]->jln_wp_sppt }}</td>
            </tr>
            <tr>
                <th>OBJEK PAJAK</th>
                <td></td>
                <td>Kecamatan: {{ $wajibPajak[0]->nm_kecamatan }}, Kelurahan: {{ $wajibPajak[0]->nm_kelurahan }}</td>
            </tr>
        </table>
        <div class="table-responsive">
            <table class="table table-hover table-bordered table-stripped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>TAHUN</th>
                        <th>LT</th>
                        <th>LB</th>
                        <th>PBB POKOK</th>
                        <th>DENDA</th>
                        <th>POKOK+DENDA</th>
                        <th>TLH DIBAYAR</th>
                        <th>-/+ BAYAR</th>
                        <th>KETERANGAN</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($informasiPajak['data'] as $key => $row)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $row['thn_pajak_sppt'] }}</td>
                        <td>{{ $row['luas_bumi_sppt'] }}</td>
                        <td>{{ $row['luas_bng_sppt'] }}</td>
                        <td>{{ $row['pbb_yg_harus_dibayar_sppt'] }}</td>
                        <td>{{ $row['denda'] }}</td>
                        <td>{{ $row['jml_harus_dibayar'] }}</td>
                        <td>{{ $row['jml_sppt_yg_dibayar'] }}</td>
                        <td>{{ $row['kurleb_bayar'] }}</td>
                        <td>{{ $row['status_pembayaran_sppt'] == 1 ? 'Lunas':'Belum Dibayar' }}</td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="10" class="text-center">Tidak ada data</td>
                    </tr>
                    @endforelse
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="9">JUMLAH YANG BELUM DIBAYAR</td>
                        <td>{{ $informasiPajak['total']['total_harus_dibayar'] }}</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</body>
</html>