<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Rekap Per Wilayah</title>
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous"> -->
    
    <style>
        body { font-family: 'Helvetica'; font-size: 12px; }
        .table, .table th, .table td { border: 0.2px solid #e9ecef; border-collapse: collapse; padding: 5px; }
        .table-noborder, .table-noborder th, .table-noborder td { border: 0; border-collapse: collapse; padding: 5px; }
        .mb-1 { margin-bottom: 10px;}
        .mt-1 { margin-top: 10px;}
        .mt-2 { margin-top: 20px;}
        .text-header {text-align: center;}
        .text-header h1{ font-size: 14px; margin: 0;}
        .text-header h2{ font-size: 12px; margin: 8px 0 0;}
        .text-header h3{ font-size: 10px; font-weight: normal;}
        .img-logo { width: 40px; height: auto; float: left; margin-right: 10px;}
        tr.kelurahan span {display: block;margin-bottom: 10px;}
        tr.kelurahan td span:last-child {margin-bottom: 0 !important;}
        hr.double {border-top: 3px double #8c8b8b; width: 70%; margin-bottom: 20px;}
    </style>
</head>
<body>
    <div>
        <table class="table-noborder mb-1" width="100%">
            <thead>
                <tr>
                    <td class="text-header" colspan="6" style="text-align: center">
                        <h1>PEMERINTAH KABUAPATEN TAKALAR</h1>
                        <h1>BADAN PEMERIKSA KEUANGAN DAERAH</h1>
                        <hr class="double">
                        <h2>DAFTAR LAPORAN PENERIMAAN PAJAK BUMI DAN BANGUNAN PERDESAAN DAN PERKOTAAN</h2>
                        <h3>WILAYAH KECAMATAN : MANGARA BOMBANG, KELURAHAN : SEMUA KELURAHAN</h3>
                        <h3>PERIODE : {{ $start }} Sd {{ $end }}</h3>
                    </td>
                </tr>
            </thead>
        </table>
        <table class="table mt-2" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>WILAYAH KELURAHAN</th>
                    <th>STTS</th>
                    <th>PBB POKOK</th>
                    <th>DENDA</th>
                    <th>JUMLAH</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($pembayaran as $key => $row)
                <tr>
                    <td width="2%">{{ $key+1 }}</td>
                    <td>{{ $row->nm_kelurahan }}</td>
                    <td>{{ number_format($row->jumlah_stts) }}</td>
                    <td>{{ number_format($row->jumlah_pbb) }}</td>
                    <td>{{ number_format($row->jumlah_denda) }}</td>
                    <td>{{ number_format($row->jumlah) }}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="6" class="text-center">Tidak ada data</td>
                </tr>
                @endforelse
            </tbody>
            <tfoot>
                <tr align="left">
                    <td colspan="2">JUMLAH</td>
                    <td>{{ number_format($stts) }}</td>
                    <td>{{ number_format($pbb) }}</td>
                    <td>{{ number_format($denda) }}</td>
                    <td>{{ number_format($pbb + $denda) }}</td>
                </tr>
            </tfoot>
        </table>
    </div>
</body>
</html>