<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Rekap Per Wilayah</title>
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous"> -->
    
    <style>
        body { font-family: 'Helvetica'; font-size: 12px; }
        .table, .table th, .table td { border: 0.2px solid #e9ecef; border-collapse: collapse; padding: 5px; }
        .table-noborder, .table-noborder th, .table-noborder td { border: 0; border-collapse: collapse; padding: 5px; }
        .mb-1 { margin-bottom: 10px;}
        .mt-1 { margin-top: 10px;}
        .mt-2 { margin-top: 20px;}
        .text-header {text-align: center;}
        .text-header h1{ font-size: 14px; margin: 0;}
        .text-header h2{ font-size: 12px; margin: 8px 0 0;}
        .text-header h3{ font-size: 10px; font-weight: normal;}
        .img-logo { width: 40px; height: auto; float: left; margin-right: 10px;}
        tr.kelurahan span {display: block;margin-bottom: 10px;}
        tr.kelurahan td span:last-child {margin-bottom: 0 !important;}
        hr.double {border-top: 3px double #8c8b8b; width: 70%; margin-bottom: 20px;}
    </style>
</head>
<body>
    <div>
        <table class="table-noborder mb-1" width="100%">
            <thead>
                <tr>
                    <td colspan="4" class="text-header" style="text-align: center">
                        <h1>PEMERINTAH KABUAPATEN TAKALAR</h1>
                        <h1>BADAN PEMERIKSA KEUANGAN DAERAH</h1>
                        <hr class="double">
                        <h2>DAFTAR REKAPITULASI PENERIMAAN PAJAK BUMI DAN BANGUNAN PERDESAAN DAN PERKOTAAN</h2>
                        <h2>PERIODE TAHUN 2020</h2>
                    </td>
                </tr>
            </thead>
        </table>
        <table class="table mt-4" width="100%">
            <thead class="text-center">
                <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">Wilayah</th>
                    <th colspan="2">Ketetapan</th>
                    <th colspan="4">Realisasi</th>
                    {{-- <th rowspan="2">%</th> --}}
                </tr>
                <tr>
                    <th>STTS (Lbr)</th>
                    <th>Jumlah</th>
                    <th>STTS (Lbr)</th>
                    <th>Pokok</th>
                    <th>Denda</th>
                    <th>Jumlah</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($penerimaan['data'] as $key => $row)
                    <tr class="kecamatan">
                        <td width="3%" valign="top">{{ $key+1 }}</td>
                        <td>{{ $row['nm_kecamatan'] }}</td>
                        <td>{{ number_format($row['items']['stts_ketetapan']) }}</td>
                        <td>{{ number_format($row['items']['ketetapan']) }}</td>
                        <td>{{ number_format($row['items']['stts']) }}</td>
                        <td>{{ number_format($row['items']['pbb']) }}</td>
                        <td>{{ number_format($row['items']['denda']) }}</td>
                        <td>{{ number_format($row['items']['pbb'] + $row['items']['denda']) }}</td>
                        {{-- <td>0</td> --}}
                    </tr>
                    <tr class="kelurahan">
                        <td valign="top"></td>
                        <td>
                            @foreach ($row['kelurahan'] as $kel)
                            <span>{{ $kel }}</span> <br>
                            @endforeach
                        </td>
                        <td>
                            @foreach ($row['data'] as $d)
                            <span>{{ number_format($d['stts_ketetapan']) }}</span> <br>
                            @endforeach
                        </td>
                        <td>
                            @foreach ($row['data'] as $d)
                            <span>{{ number_format($d['ketetapan']) }}</span> <br>
                            @endforeach
                        </td>
                        <td>
                            @foreach ($row['data'] as $d)
                            <span>{{ number_format($d['stts']) }}</span> <br>
                            @endforeach
                        </td>
                        <td>
                            @foreach ($row['data'] as $d)
                            <span>{{ number_format($d['pbb']) }}</span> <br>
                            @endforeach
                        </td>
                        <td>
                            @foreach ($row['data'] as $d)
                            <span>{{ number_format($d['denda']) }}</span> <br>
                            @endforeach
                        </td>
                        <td>
                            @foreach ($row['data'] as $d)
                            <span>{{ number_format($d['pbb'] + $d['denda']) }}</span> <br>
                            @endforeach
                        </td>
                        {{-- <td>
                            <span>0</span>
                            <span>0</span>
                            <span>0</span>
                            <span>0</span>
                            <span>0</span>
                        </td> --}}
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="2">Jumlah</th>
                    <th>{{ number_format($penerimaan['meta']['stts_ketetapan']) }}</th>
                    <th>{{ number_format($penerimaan['meta']['ketetapan']) }}</th>
                    <th>{{ number_format($penerimaan['meta']['stts']) }}</th>
                    <th>{{ number_format($penerimaan['meta']['pbb']) }}</th>
                    <th>{{ number_format($penerimaan['meta']['denda']) }}</th>
                    <th>{{ number_format($penerimaan['meta']['pbb'] + $penerimaan['meta']['denda']) }}</th>
                    {{-- <th>0</th> --}}
                </tr>
            </tfoot>
        </table>
    </div>
</body>
</html>