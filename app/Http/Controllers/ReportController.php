<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\ReportPerUser;
use App\Exports\ReportPenerimaan;
use App\Exports\ReportPenerimaanBerjalan;
use App\Exports\ReportWilayah;
use App\Exports\ReportTunggakan;
use App\Exports\ReportTempatBayar;
use App\PembayaranSppt;
use Carbon\Carbon;
use App\Pegawai;
use Validator;
use App\Sppt;
use App\TP;
use Excel;
use File;
use PDF;
use DB;

class ReportController extends Controller
{
    public function getPdfFile($filename)
    {
        return response()->file(public_path('storage/' . $filename), ['Content-Type' => 'application/pdf']);
    }

    public function getTP()
    {
		$tp = TP::select('kd_kanwil', 'kd_kppbb', 'kd_bank_tunggal', 'kd_bank_persepsi', 'kd_tp', 'nm_tp')->orderBy('nm_tp')->get();
        return response()->json(['status' => 'success', 'data' => $tp], 200);
	}
	
	public function getPegawai()
	{
		$pegawai = Pegawai::orderBy('nm_pegawai', 'ASC')->get();
		return response()->json(['status' => 'success', 'data' => $pegawai]);
	}

	//LAPORAN PER WILAYAH
    public function reportPerWilayah(Request $request)
    {
        $this->validate($request, [
            'kd_kecamatan' => 'required',
			'kd_kelurahan' => 'nullable',
			'start' => 'required|date',
			'end' => 'required|date'
            // 'thn_pajak_sppt' => 'required'
        ]);
		
		try {
            // $pembayaran_sppt = $this->queryReportPerWilayah($request->thn_pajak_sppt, $request->kd_kecamatan, $request->kd_kelurahan);
            $pembayaran_sppt = $this->queryReportPerWilayah($request->start, $request->end, $request->kd_kecamatan, $request->kd_kelurahan);

			$getTotal = $pembayaran_sppt->get();

			$stts = 0;
			$pbb = 0;
			$denda = 0;

			if (count($getTotal) > 0) {
				$stts = $getTotal->sum(function($item) {
					return $item->jumlah_stts;
				});
				$pbb = $getTotal->sum(function($item) {
					return $item->jumlah_pbb;
				});
				$denda = $getTotal->sum(function($item) {
					return $item->jumlah_denda;
				});
			}
			
			$meta = [
				'stts' => $stts,
				'pbb_pokok' => $pbb,
				'denda' => $denda,
				'jumlah' => $pbb + $denda
			];

			return response()->json([
				'status' => 'success',
				// 'data' => $pembayaran_sppt->paginate(10),
				'data' => $getTotal,
				'meta' => $meta
			]);
		
        } catch (\Exception $e) {
            return response()->json([
				'status' => 'error',
				'message' => $e->getMessage()
			], 400);
        }
	}

	// private function queryReportPerWilayah($thn_pajak_sppt, $kd_kecamatan, $kd_kelurahan)
	private function queryReportPerWilayah($start, $end, $kd_kecamatan, $kd_kelurahan)
	{
		return PembayaranSppt::join('ref_kecamatan', function($join) {
			$join->on('ref_kecamatan.kd_propinsi', '=', 'pembayaran_sppt.kd_propinsi')
			->on('ref_kecamatan.kd_dati2', '=', 'pembayaran_sppt.kd_dati2')
			->on('ref_kecamatan.kd_kecamatan', '=', 'pembayaran_sppt.kd_kecamatan');
		})
		->join('ref_kelurahan', function ($join) {
			$join->on('ref_kelurahan.kd_propinsi', '=', 'pembayaran_sppt.kd_propinsi')
			->on('ref_kelurahan.kd_dati2', '=', 'pembayaran_sppt.kd_dati2')
			->on('ref_kelurahan.kd_kecamatan', '=', 'pembayaran_sppt.kd_kecamatan')
			->on('ref_kelurahan.kd_kelurahan', '=', 'pembayaran_sppt.kd_kelurahan');
		})
		->selectRaw(
			'pembayaran_sppt.kd_propinsi,pembayaran_sppt.kd_dati2,pembayaran_sppt.kd_kelurahan,pembayaran_sppt.kd_kecamatan,COUNT(*) as jumlah_stts,SUM(pembayaran_sppt.denda_sppt) as jumlah_denda,SUM(pembayaran_sppt.jml_sppt_yg_dibayar) as jumlah,SUM(pembayaran_sppt.jml_sppt_yg_dibayar) - SUM(pembayaran_sppt.denda_sppt) as jumlah_pbb,ref_kecamatan.nm_kecamatan,ref_kelurahan.nm_kelurahan'
		)
		->groupBy(
			'pembayaran_sppt.kd_propinsi',
			'pembayaran_sppt.kd_dati2',
			'pembayaran_sppt.kd_kelurahan',
			'pembayaran_sppt.kd_kecamatan', 
			'ref_kecamatan.nm_kecamatan', 
			'ref_kelurahan.nm_kelurahan'
		)
		// ->whereRaw('Extract(YEAR from pembayaran_sppt.tgl_pembayaran_sppt)=' . $thn_pajak_sppt)
		->whereBetween('pembayaran_sppt.tgl_pembayaran_sppt', [$start, $end])
		->when($kd_kelurahan, function($pembayaran) use($kd_kelurahan) {
			$pembayaran->where('pembayaran_sppt.kd_kelurahan', $kd_kelurahan);
		})
		->where('pembayaran_sppt.kd_kecamatan', $kd_kecamatan);
	}

	public function reportPerWilayahPDF(Request $request)
	{
		$path = storage_path('app/public/report');
        if (!File::isDirectory($path)) {
            File::makeDirectory($path);
        }
	 
		$start = $request->start;
		$end = $request->end;
		// $pembayaran = $this->queryReportPerWilayah($request->thn_pajak_sppt, $request->kd_kecamatan, $request->kd_kelurahan)->get();
		$pembayaran = $this->queryReportPerWilayah($request->start, $request->end, $request->kd_kecamatan, $request->kd_kelurahan)->get();

		$stts = $pembayaran->sum(function($item) {
			return $item->jumlah_stts;
		});
		$pbb = $pembayaran->sum(function($item) {
			return $item->jumlah_pbb;
		});
		$denda = $pembayaran->sum(function($item) {
			return $item->jumlah_denda;
		});

		$pdf = PDF::loadView('reports.per_wilayah', compact('pembayaran', 'stts', 'pbb', 'denda', 'start', 'end'))
			->setPaper('legal', 'potrait');
		$pdf->save($path . '/per_wilayah.pdf');
		
		// return response()->file(public_path('storage/report/per_wilayah.pdf'), ['Content-Type' => 'application/pdf']);
        return response()->json([
            'status' => 'success',
            'data' => asset('storage/report/per_wilayah.pdf')
        ]);
	}

	public function reportPerWilayahExcel(Request $request)
	{
		$path = storage_path('app/public/report');
        if (!File::isDirectory($path)) {
            File::makeDirectory($path);
        }
	 
		$start = $request->start;
		$end = $request->end;
		// $pembayaran = $this->queryReportPerWilayah($request->thn_pajak_sppt, $request->kd_kecamatan, $request->kd_kelurahan)->get();
		$pembayaran = $this->queryReportPerWilayah($request->start, $request->end, $request->kd_kecamatan, $request->kd_kelurahan)->get();

		$stts = $pembayaran->sum(function($item) {
			return $item->jumlah_stts;
		});
		$pbb = $pembayaran->sum(function($item) {
			return $item->jumlah_pbb;
		});
		$denda = $pembayaran->sum(function($item) {
			return $item->jumlah_denda;
		});


		Excel::store(new ReportWilayah($pembayaran, $stts, $pbb, $denda, $start, $end), 'public/report/wilayah.xlsx');
        return response()->json([
            'status' => 'success',
            'data' => asset('storage/report/wilayah.xlsx')
        ]);
	}
	
	//LAPORAN TEMPAT PEMBAYARAN
	public function reportTP(Request $request)
	{
		$validate = Validator::make($request->all(), [
            'start' => 'required|date',
			'end' => 'required|date',
			'kd_tp' => 'nullable|numeric|digits:10'
        ]);
        
        if ($validate->fails()) {
            return response()->json($validate->errors(), 422);
		}
		
		$start = Carbon::parse($request->start)->format('d/m/Y');
		$end = Carbon::parse($request->end)->format('d/m/Y');
		$pembayaran = $this->queryReportTp($request->kd_tp, $request->start, $request->end);

		$getTotal = $pembayaran->get();
		$stts = $getTotal->sum(function($item) {
			return $item->jumlah_stts;
		});
		$pbb = $getTotal->sum(function($item) {
			return $item->jumlah_pbb;
		});
		$denda = $getTotal->sum(function($item) {
			return $item->jumlah_denda;
		});
		$meta = [
			'stts' => $stts,
			'pbb_pokok' => $pbb,
			'denda' => $denda,
			'jumlah' => $pbb + $denda
		];

		return response()->json([
			'status' => 'success',
			'data' => $pembayaran->paginate(10),
			'meta' => $meta
		]);
	}

	public function queryReportTp($kd_tp, $start, $end)
	{
		$filterDate = 'To_Date(' . $start . ', "yyyy/mm/dd") AND To_Date(' . $end . ', "yyyy/mm/dd"))';
		if (env('DB_CONNECTION') == 'mysql') {
			$filterDate = "'$start' AND '$end'";
		}

		$pembayaran = PembayaranSppt::join('tempat_pembayaran', function($join) {
			$join->on('tempat_pembayaran.kd_tp', '=', 'pembayaran_sppt.kd_tp')
				->on('tempat_pembayaran.kd_bank_persepsi', '=', 'pembayaran_sppt.kd_bank_persepsi')
				->on('tempat_pembayaran.kd_bank_tunggal', '=', 'pembayaran_sppt.kd_bank_tunggal')
				->on('tempat_pembayaran.kd_kppbb', '=', 'pembayaran_sppt.kd_kppbb_bank')
				->on('tempat_pembayaran.kd_kanwil', '=', 'pembayaran_sppt.kd_kanwil_bank');
			})->selectRaw('tempat_pembayaran.nm_tp,COUNT(*) as jumlah_stts,SUM(pembayaran_sppt.denda_sppt) as jumlah_denda,SUM(pembayaran_sppt.jml_sppt_yg_dibayar) as jumlah,SUM(pembayaran_sppt.jml_sppt_yg_dibayar) - SUM(pembayaran_sppt.denda_sppt) as jumlah_pbb')
			->groupBy('tempat_pembayaran.nm_tp')
			->when($kd_tp, function($pembayaran) use($kd_tp) {
				$pembayaran->where('tempat_pembayaran.kd_kanwil', $kd_tp[0] . $kd_tp[1])
					->where('tempat_pembayaran.kd_kppbb', $kd_tp[2] . $kd_tp[3])
					->where('tempat_pembayaran.kd_bank_tunggal', $kd_tp[4] . $kd_tp[5])
					->where('tempat_pembayaran.kd_bank_persepsi', $kd_tp[6] . $kd_tp[7])
					->where('tempat_pembayaran.kd_tp', $kd_tp[8] . $kd_tp[9]);
			});
		
		if (env('DB_CONNECTION') == 'mysql') {
			$pembayaran = $pembayaran->whereBetween('pembayaran_sppt.tgl_pembayaran_sppt', [$start, $end]);
		} else {
			$pembayaran = $pembayaran->whereRaw("(pembayaran_sppt.tgl_pembayaran_sppt BETWEEN To_Date('$start', 'yyyy/mm/dd') AND To_Date('$end', 'yyyy/mm/dd'))");
		}
		return $pembayaran;
	}

	public function reportTpPDF(Request $request)
	{
		$path = storage_path('app/public/report');
        if (!File::isDirectory($path)) {
            File::makeDirectory($path);
        }
	 
		$start = $request->start;
		$end = $request->end;
		$pembayaran = $this->queryReportTp($request->kd_tp, $request->start, $request->end)->get();

		$stts = $pembayaran->sum(function($item) {
			return $item->jumlah_stts;
		});
		$pbb = $pembayaran->sum(function($item) {
			return $item->jumlah_pbb;
		});
		$denda = $pembayaran->sum(function($item) {
			return $item->jumlah_denda;
		});

		$pdf = PDF::loadView('reports.tp', compact('pembayaran', 'stts', 'pbb', 'denda', 'start', 'end'))
			->setPaper('legal', 'potrait');
		$pdf->save($path . '/tempat_pembayaran.pdf');
		
		// return response()->file(public_path('storage/report/tempat_pembayaran.pdf'), ['Content-Type' => 'application/pdf']);
        return response()->json([
            'status' => 'success',
            'data' => asset('storage/report/tempat_pembayaran.pdf')
        ]);
	}

	public function reportPerTpExcel(Request $request)
	{
		$path = storage_path('app/public/report');
        if (!File::isDirectory($path)) {
            File::makeDirectory($path);
        }
	 
		$start = $request->start;
		$end = $request->end;
		$pembayaran = $this->queryReportTp($request->kd_tp, $request->start, $request->end)->get();

		$stts = $pembayaran->sum(function($item) {
			return $item->jumlah_stts;
		});
		$pbb = $pembayaran->sum(function($item) {
			return $item->jumlah_pbb;
		});
		$denda = $pembayaran->sum(function($item) {
			return $item->jumlah_denda;
		});

		Excel::store(new ReportTempatBayar($pembayaran, $stts, $pbb, $denda, $start, $end), 'public/report/tp.xlsx');
        return response()->json([
            'status' => 'success',
            'data' => asset('storage/report/tp.xlsx')
        ]);
	}

	//LAPORAN TUNGGAKAN
	public function reportTunggakan(Request $request)
    {
		$validate = Validator::make($request->all(), [
            'start_year' => 'required|numeric|digits:4',
			'end_year' => 'required|numeric|digits:4',
			'kd_kecamatan' => 'nullable|numeric|digits:7',
			'kd_kelurahan' => 'nullable|numeric|digits:10',
        ]);
        
        if ($validate->fails()) {
            return response()->json($validate->errors(), 422);
		}
		
		try {
			$sppt = $this->queryReportTunggakan($request->kd_kecamatan, $request->kd_kelurahan, $request->start_year, $request->end_year);
			return response()->json(['status' => 'success', 'data' => $sppt]);
		} catch (\Exception $e) {
			return $e->getMessage();
		}
	}

	private function queryReportTunggakan($kd_kecamatan, $kd_kelurahan, $startYear, $endYear)
	{
		$sppt = Sppt::selectRaw('
			ref_kelurahan.nm_kelurahan, COUNT(*) as stts, coalesce(SUM(sppt.pbb_yg_harus_dibayar_sppt), 0) as pbb,
			SUM(CASE
				WHEN TIMESTAMPDIFF(MONTH, sppt.tgl_jatuh_tempo_sppt, CURDATE()) > 24
				THEN sppt.pbb_yg_harus_dibayar_sppt * 24 * 0.2
				ELSE sppt.pbb_yg_harus_dibayar_sppt * TIMESTAMPDIFF(MONTH, sppt.tgl_jatuh_tempo_sppt, CURDATE()) * 0.2
			END) as denda
		')->join('ref_kelurahan', function($join) {
				$join->on('ref_kelurahan.kd_propinsi', 'sppt.kd_propinsi')
				->on('ref_kelurahan.kd_dati2', 'sppt.kd_dati2')
				->on('ref_kelurahan.kd_kecamatan', 'sppt.kd_kecamatan')
				->on('ref_kelurahan.kd_kelurahan', 'sppt.kd_kelurahan');
			})
			->where('sppt.status_pembayaran_sppt', 0)
			->whereBetween('sppt.thn_pajak_sppt', [$startYear, $endYear])
			->when($kd_kecamatan, function($query) use($kd_kecamatan) {
				$query->where(function($q) use($kd_kecamatan) {
					$q->where('sppt.kd_propinsi', $kd_kecamatan[0] . $kd_kecamatan[1])
					->where('sppt.kd_dati2', $kd_kecamatan[2] . $kd_kecamatan[3])
					->where('sppt.kd_kecamatan', $kd_kecamatan[4] . $kd_kecamatan[5] . $kd_kecamatan[6]);
				});
			})
			->when($kd_kelurahan, function($query) use($kd_kelurahan) {
				$query->where(function($q) use($kd_kelurahan) {
					$q->where('sppt.kd_propinsi', $kd_kelurahan[0] . $kd_kelurahan[1])
					->where('sppt.kd_dati2', $kd_kelurahan[2] . $kd_kelurahan[3])
					->where('sppt.kd_kecamatan', $kd_kelurahan[4] . $kd_kelurahan[5] . $kd_kelurahan[6])
					->where('sppt.kd_kelurahan', $kd_kelurahan[7] . $kd_kelurahan[8] . $kd_kelurahan[9]);
				});
			})
			->groupBy('nm_kelurahan')
			->get();
		return $sppt;
	}

	public function reportTunggakanPdf(Request $request)
	{
		$path = storage_path('app/public/report');
        if (!File::isDirectory($path)) {
            File::makeDirectory($path);
        }
	 
		$start = $request->start_year;
		$end = $request->end_year;
		$sppt = $this->queryReportTunggakan($request->kd_kecamatan, $request->kd_kelurahan, $start, $end);

		$stts = 0;
		$pbb = 0;
		$denda = 0;
		// $stts = $pembayaran->sum(function($item) {
		// 	return $item->jumlah_stts;
		// });
		// $pbb = $pembayaran->sum(function($item) {
		// 	return $item->jumlah_pbb;
		// });
		// $denda = $pembayaran->sum(function($item) {
		// 	return $item->jumlah_denda;
		// });

		$pdf = PDF::loadView('reports.tunggakan', compact('sppt', 'stts', 'pbb', 'denda', 'start', 'end'))
			->setPaper('legal', 'potrait');
		$pdf->save($path . '/tunggakan.pdf');
		
		// return response()->file(public_path('storage/report/per_wilayah.pdf'), ['Content-Type' => 'application/pdf']);
        return response()->json([
            'status' => 'success',
            'data' => asset('storage/report/tunggakan.pdf')
        ]);
	}

	public function reportTunggakannExcel(Request $request)
	{
		$path = storage_path('app/public/report');
        if (!File::isDirectory($path)) {
            File::makeDirectory($path);
        }
	 
		$start = $request->start_year;
		$end = $request->end_year;
		$sppt = $this->queryReportTunggakan($request->kd_kecamatan, $request->kd_kelurahan, $start, $end);

		Excel::store(new ReportTunggakan($sppt, $start, $end), 'public/report/tunggakan.xlsx');
        return response()->json([
            'status' => 'success',
            'data' => asset('storage/report/tunggakan.xlsx')
        ]);
	}

	// public function queryReportTunggakan($kd_kecamatan, $start_year, $end_year)
	// {
	// 	return DB::table("ref_kelurahan")
	// 	->join("ref_kecamatan", function($join) {
	// 		$join->on("ref_kecamatan.kd_kecamatan", "=", "ref_kelurahan.kd_kecamatan");
	// 		$join->on("ref_kecamatan.kd_dati2", "=", "ref_kelurahan.kd_dati2");
	// 		$join->on("ref_kecamatan.kd_propinsi", "=", "ref_kelurahan.kd_propinsi");					
	// 	})
	// 	->join("ref_dati2", function($join) {
	// 		$join->on("ref_dati2.kd_dati2", "=", "ref_kelurahan.kd_dati2");
	// 		$join->on("ref_dati2.kd_propinsi", "=", "ref_kelurahan.kd_propinsi");					
	// 	})
	// 	->join("ref_propinsi", function($join) {
	// 		$join->on("ref_propinsi.kd_propinsi", "=", "ref_kelurahan.kd_propinsi");					
	// 	})
	// 	// ->where("ref_kelurahan.kd_propinsi", $request->kd_propinsi)
	// 	// ->where("ref_kelurahan.kd_dati2", $request->kd_dati2)
	// 	->where("ref_kelurahan.kd_kecamatan", $kd_kecamatan)
	// 	->orderBy("ref_kelurahan.kd_propinsi") 
	// 	->orderBy("ref_kelurahan.kd_dati2")
	// 	->orderBy("ref_kelurahan.kd_kecamatan") 
	// 	->orderBy("ref_kelurahan.kd_kelurahan")
	// 	->groupBy(
	// 		"ref_kelurahan.kd_propinsi", "ref_propinsi.nm_propinsi",
	// 		"ref_kelurahan.kd_dati2", "ref_dati2.nm_dati2",
	// 		"ref_kelurahan.kd_kecamatan", "ref_kecamatan.nm_kecamatan",
	// 		"ref_kelurahan.kd_kelurahan", "ref_kelurahan.nm_kelurahan"
	// 	)				
	// 	->select(
	// 		"ref_kelurahan.kd_propinsi", "ref_propinsi.nm_propinsi",
	// 		"ref_kelurahan.kd_dati2", "ref_dati2.nm_dati2",
	// 		"ref_kelurahan.kd_kecamatan", "ref_kecamatan.nm_kecamatan",
	// 		"ref_kelurahan.kd_kelurahan", "ref_kelurahan.nm_kelurahan",
	// 		DB::raw("
	// 			(SELECT 
	// 				Count(a.status_pembayaran_sppt) 
	// 				FROM sppt a
	// 				WHERE 
	// 				a.thn_pajak_sppt >= '$start_year' AND
	// 				a.thn_pajak_sppt <= '$end_year' AND 
	// 				a.status_pembayaran_sppt = 0 AND
	// 				a.kd_propinsi = ref_kelurahan.kd_propinsi AND
	// 				a.kd_dati2 = ref_kelurahan.kd_dati2 AND 
	// 				a.kd_kecamatan = ref_kelurahan.kd_kecamatan AND 
	// 				a.kd_kelurahan = ref_kelurahan.kd_kelurahan
	// 			) as jumlah_stts,
	// 			(SELECT 
	// 				Sum(b.pbb_yg_harus_dibayar_sppt)
	// 				FROM sppt b
	// 				WHERE 
	// 				b.thn_pajak_sppt >= '$start_year' AND
	// 				b.thn_pajak_sppt <= '$end_year' AND 
	// 				b.status_pembayaran_sppt = 0 AND 
	// 				b.kd_propinsi = ref_kelurahan.kd_propinsi AND
	// 				b.kd_dati2 = ref_kelurahan.kd_dati2 AND 
	// 				b.kd_kecamatan = ref_kelurahan.kd_kecamatan AND 
	// 				b.kd_kelurahan = ref_kelurahan.kd_kelurahan 
	// 			) as jumlah_pbb_pokok,
	// 			(SELECT 
	// 				Sum(hit_bulan_denda(To_Char(SYSDATE, 'dd-mm-yyyy'), To_Char(c.tgl_jatuh_tempo_sppt, 'dd-mm-yyyy'), c.pbb_yg_harus_dibayar_sppt))
	// 				FROM sppt c
	// 				WHERE 
	// 				c.thn_pajak_sppt >= '$start_year' AND
	// 				c.thn_pajak_sppt <= '$end_year' AND 
	// 				c.status_pembayaran_sppt = 0 AND
	// 				c.kd_propinsi = ref_kelurahan.kd_propinsi AND
	// 				c.kd_dati2 = ref_kelurahan.kd_dati2 AND 
	// 				c.kd_kecamatan = ref_kelurahan.kd_kecamatan AND 
	// 				c.kd_kelurahan = ref_kelurahan.kd_kelurahan
	// 			) as jumlah_denda,
	// 			(SELECT 
	// 				Sum(hit_bulan_denda(To_Char(SYSDATE, 'dd-mm-yyyy'), To_Char(d.tgl_jatuh_tempo_sppt, 'dd-mm-yyyy'), d.pbb_yg_harus_dibayar_sppt)) +
	// 					Sum(d.pbb_yg_harus_dibayar_sppt)
	// 				FROM sppt d
	// 				WHERE 
	// 				d.thn_pajak_sppt >= '$start_year' AND
	// 				d.thn_pajak_sppt <= '$end_year' AND 
	// 				d.status_pembayaran_sppt = 0 AND 
	// 				d.kd_propinsi = ref_kelurahan.kd_propinsi AND
	// 				d.kd_dati2 = ref_kelurahan.kd_dati2 AND 
	// 				d.kd_kecamatan = ref_kelurahan.kd_kecamatan AND 
	// 				d.kd_kelurahan = ref_kelurahan.kd_kelurahan 
	// 			) as jumlah_sppt

	// 		")
	// 	)->get();
	// }

	//LAPORAN PER USER
	public function reportPerUser(Request $request)
	{
		$validate = Validator::make($request->all(), [
            'start' => 'required|date',
			'end' => 'required|date',
			'nip' => 'nullable|string'
        ]);
        
        if ($validate->fails()) {
            return response()->json($validate->errors(), 422);
		}

		$pembayaran = $this->queryReportPerUser($request->start, $request->end, $request->nip, 'data');
		return response()->json(['status' => 'success', 'data' => $pembayaran]);
	}

	private function queryReportPerUser($start, $end, $nip, $type)
	{
		$pembayaran = PembayaranSppt::selectRaw('pegawai.nm_pegawai, tempat_pembayaran.nm_tp, COUNT(*) as jumlah_stts, SUM(pembayaran_sppt.jml_sppt_yg_dibayar - pembayaran_sppt.denda_sppt) as pbb_pokok, SUM(pembayaran_sppt.denda_sppt) as jumlah_denda')
			->join('pegawai', 'pegawai.nip', '=', 'pembayaran_sppt.nip_rekam_byr_sppt')
			->join('tempat_pembayaran', function($join) {
				$join->on('tempat_pembayaran.kd_kanwil', '=', 'pembayaran_sppt.kd_kanwil_bank')
				->on('tempat_pembayaran.kd_kppbb', '=', 'pembayaran_sppt.kd_kppbb_bank')
				->on('tempat_pembayaran.kd_bank_tunggal', '=', 'pembayaran_sppt.kd_bank_tunggal')
				->on('tempat_pembayaran.kd_bank_persepsi', '=', 'pembayaran_sppt.kd_bank_persepsi')
				->on('tempat_pembayaran.kd_tp', '=', 'pembayaran_sppt.kd_tp');
			})
			->whereBetween('tgl_pembayaran_sppt', [$start, $end])
			->when($nip, function($pembayaran) use($nip) {
				$format_nip = trim(str_replace('+', '', $nip));
				$pembayaran->where('pembayaran_sppt.nip_rekam_byr_sppt', $format_nip);
			})
			->groupBy('pegawai.nm_pegawai')
			->groupBy('tempat_pembayaran.nm_tp');

		return $pembayaran->get();
		return $type == 'pdf' ? $pembayaran->get():$pembayaran->paginate(10);
	}

	public function reportPerUserPDF(Request $request)
	{
		$path = storage_path('app/public/report');
        if (!File::isDirectory($path)) {
            File::makeDirectory($path);
        }
	 
		$start = $request->start;
		$end = $request->end;
		$pembayaran = $this->queryReportPerUser($request->start, $request->end, $request->nip, 'data');

		$total_stts = $pembayaran->sum(function($item) {
			return $item->jumlah_stts;
		});
		$total_pbb = $pembayaran->sum(function($item) {
			return $item->pbb_pokok;
		});
		$total_denda = $pembayaran->sum(function($item) {
			return $item->jumlah_denda;
		});

		$pdf = PDF::loadView('reports.per_user', compact('pembayaran', 'total_stts', 'total_pbb', 'total_denda', 'start', 'end'))
			->setPaper('legal', 'potrait');
		$pdf->save($path . '/per_user.pdf');
		
		// return response()->file(public_path('storage/report/per_user.pdf'), ['Content-Type' => 'application/pdf']);
        return response()->json([
            'status' => 'success',
            'data' => asset('storage/report/per_user.pdf')
        ]);
	}

	public function reportPerUserExcel(Request $request)
	{
		$path = storage_path('app/public/report');
        if (!File::isDirectory($path)) {
            File::makeDirectory($path);
        }
	 
		$start = $request->start;
		$end = $request->end;
		$pembayaran = $this->queryReportPerUser($request->start, $request->end, $request->nip, 'data');

		Excel::store(new ReportPerUser($pembayaran, $start, $end), 'public/report/per_user.xlsx');
        return response()->json([
            'status' => 'success',
            'data' => asset('storage/report/per_user.xlsx')
        ]);
	}

	//LAPORAN PENERIMAAN
	public function reportPenerimaan()
	{
		$data = $this->queryPenerimaan();
		return response()->json(['status' => 'success', 'data' => $data['data'], 'meta' => $data['meta']]);
	}

	private function queryPenerimaan()
	{
		$endYear = Carbon::now()->format('Y');
		$startYear = $endYear - 4;
		
		$query = PembayaranSppt::selectRaw('
			Extract(YEAR from pembayaran_sppt.tgl_pembayaran_sppt) as year, count(*) as stts, coalesce(SUM(pembayaran_sppt.jml_sppt_yg_dibayar), 0) as pbb,
			ref_kecamatan.nm_kecamatan, ref_kelurahan.nm_kelurahan
		')
			->join('ref_kecamatan', function($join) {
				$join->on('ref_kecamatan.kd_propinsi', '=', 'pembayaran_sppt.kd_propinsi')
				->on('ref_kecamatan.kd_dati2', '=', 'pembayaran_sppt.kd_dati2')
				->on('ref_kecamatan.kd_kecamatan', '=', 'pembayaran_sppt.kd_kecamatan');
			})
			->join('ref_kelurahan', function($join) {
				$join->on('ref_kelurahan.kd_propinsi', '=', 'pembayaran_sppt.kd_propinsi')
				->on('ref_kelurahan.kd_dati2', '=', 'pembayaran_sppt.kd_dati2')
				->on('ref_kelurahan.kd_kecamatan', '=', 'pembayaran_sppt.kd_kecamatan')
				->on('ref_kelurahan.kd_kelurahan', '=', 'pembayaran_sppt.kd_kelurahan');
			})
			->whereRaw('Extract(YEAR from pembayaran_sppt.tgl_pembayaran_sppt) BETWEEN ' . $startYear . ' AND ' . $endYear)
			->when(request()->q, function($query) {
				$q = request()->q;
				$kd_propinsi = $q[0] . $q[1];
				$kd_dati2 = $q[2] . $q[3];
				$kd_kecamatan = $q[4] . $q[5] . $q[6];

				$query->where('pembayaran_sppt.kd_propinsi', $kd_propinsi)
					->where('pembayaran_sppt.kd_dati2', $kd_dati2)
					->where('pembayaran_sppt.kd_kecamatan', $kd_kecamatan);
			})
			->groupBy('year')
			->groupBy('nm_kecamatan')
			->groupBy('nm_kelurahan')
			->orderBy('year', 'ASC')
			->get();
			
		$data = $query->groupBy('nm_kecamatan')
			->map(function($item, $index) {
				$grouped = $item->groupBy('year');
				$data = $grouped->map(function($row, $k) {
					return [
						'stts' => $row->sum('stts'),
						'pbb' => $row->sum('pbb'),
					];
				})->toArray();
				$kelurahan = array_unique($item->pluck('nm_kelurahan')->values()->all());
				return [
					'nm_kecamatan' => $index,
					'items' => $data,
					'kelurahan' => $kelurahan,
					'data' => $grouped->all()
				];
			})->values()->toArray();

		$dataTotal = $query->groupBy('year')->map(function($item, $key) {
			return [
				'stts' => $item->sum('stts'),
				'pbb' => $item->sum('pbb')
			];
		})->values()->all();
		return [
			'data' => $data,
			'meta' => $dataTotal
		];
	}

	public function reportPenerimaanPdf()
	{
		$path = storage_path('app/public/report');
        if (!File::isDirectory($path)) {
            File::makeDirectory($path);
        }
	 
		$penerimaan = $this->queryPenerimaan();

		$currentYear = Carbon::now()->format('Y');
		$headerYear = [];

		for($i = $currentYear - 4; $i < $currentYear+1; $i++) {
			$headerYear[] = $i;
		}

		$pdf = PDF::loadView('reports.penerimaan', compact('penerimaan', 'headerYear'))
			->setPaper('legal', 'landscape');
		$pdf->save($path . '/penerimaan.pdf');
		
		// return response()->file(public_path('storage/report/penerimaan.pdf'), ['Content-Type' => 'application/pdf']);
        return response()->json([
            'status' => 'success',
            'data' => asset('storage/report/penerimaan.pdf')
        ]);
	}

	public function reportPenerimaanExcel(Request $request)
	{
		$path = storage_path('app/public/report');
        if (!File::isDirectory($path)) {
            File::makeDirectory($path);
        }
	 
		$penerimaan = $this->queryPenerimaan();

		$currentYear = Carbon::now()->format('Y');
		$headerYear = [];

		for($i = $currentYear - 4; $i < $currentYear+1; $i++) {
			$headerYear[] = $i;
		}

		Excel::store(new ReportPenerimaan($penerimaan, $headerYear), 'public/report/penerimaan.xlsx');
        return response()->json([
            'status' => 'success',
            'data' => asset('storage/report/penerimaan.xlsx')
        ]);
	}

	//LAPORAN PENERIMAAN TAHUN BERJALAN
	public function reportPenerimaanBerjalan()
	{
		$data = $this->queryPenerimaanBerjalan();
		return response()->json(['status' => 'success', 'data' => $data['data'], 'meta' => $data['meta']]);
	}

	private function queryPenerimaanBerjalan()
	{
		$currentYear = Carbon::now()->format('Y');
		if (request()->year != '') {
			$currentYear = request()->year;
		}

		$sppt = Sppt::selectRaw('
			sppt.thn_pajak_sppt as year, count(*) as stts_ketetapan, coalesce(SUM(sppt.pbb_yg_harus_dibayar_sppt), 0) as ketetapan,
			ref_kecamatan.nm_kecamatan, ref_kelurahan.nm_kelurahan	
		')
			->join('ref_kecamatan', function($join) {
				$join->on('ref_kecamatan.kd_propinsi', '=', 'sppt.kd_propinsi')
				->on('ref_kecamatan.kd_dati2', '=', 'sppt.kd_dati2')
				->on('ref_kecamatan.kd_kecamatan', '=', 'sppt.kd_kecamatan');
			})
			->join('ref_kelurahan', function($join) {
				$join->on('ref_kelurahan.kd_propinsi', '=', 'sppt.kd_propinsi')
				->on('ref_kelurahan.kd_dati2', '=', 'sppt.kd_dati2')
				->on('ref_kelurahan.kd_kecamatan', '=', 'sppt.kd_kecamatan')
				->on('ref_kelurahan.kd_kelurahan', '=', 'sppt.kd_kelurahan');
			})
			->where('sppt.thn_pajak_sppt', $currentYear)
			->when(request()->q, function($query) {
				$q = request()->q;
				$kd_propinsi = $q[0] . $q[1];
				$kd_dati2 = $q[2] . $q[3];
				$kd_kecamatan = $q[4] . $q[5] . $q[6];

				$query->where('sppt.kd_propinsi', $kd_propinsi)
					->where('sppt.kd_dati2', $kd_dati2)
					->where('sppt.kd_kecamatan', $kd_kecamatan);
			})
			->groupBy('thn_pajak_sppt')
			->groupBy('nm_kecamatan')
			->groupBy('nm_kelurahan')
			->orderBy('year', 'ASC')
			->get();

		$query = PembayaranSppt::selectRaw('
			Extract(YEAR from pembayaran_sppt.tgl_pembayaran_sppt) as year, count(*) as stts, coalesce(SUM(pembayaran_sppt.jml_sppt_yg_dibayar), 0) as pbb,
			SUM(pembayaran_sppt.denda_sppt) as denda,
			ref_kecamatan.nm_kecamatan, ref_kelurahan.nm_kelurahan
		')
			->join('ref_kecamatan', function($join) {
				$join->on('ref_kecamatan.kd_propinsi', '=', 'pembayaran_sppt.kd_propinsi')
				->on('ref_kecamatan.kd_dati2', '=', 'pembayaran_sppt.kd_dati2')
				->on('ref_kecamatan.kd_kecamatan', '=', 'pembayaran_sppt.kd_kecamatan');
			})
			->join('ref_kelurahan', function($join) {
				$join->on('ref_kelurahan.kd_propinsi', '=', 'pembayaran_sppt.kd_propinsi')
				->on('ref_kelurahan.kd_dati2', '=', 'pembayaran_sppt.kd_dati2')
				->on('ref_kelurahan.kd_kecamatan', '=', 'pembayaran_sppt.kd_kecamatan')
				->on('ref_kelurahan.kd_kelurahan', '=', 'pembayaran_sppt.kd_kelurahan');
			})
			->whereRaw('Extract(YEAR from pembayaran_sppt.tgl_pembayaran_sppt)=' . $currentYear)
			->when(request()->q, function($query) {
				$q = request()->q;
				$kd_propinsi = $q[0] . $q[1];
				$kd_dati2 = $q[2] . $q[3];
				$kd_kecamatan = $q[4] . $q[5] . $q[6];

				$query->where('pembayaran_sppt.kd_propinsi', $kd_propinsi)
					->where('pembayaran_sppt.kd_dati2', $kd_dati2)
					->where('pembayaran_sppt.kd_kecamatan', $kd_kecamatan);
			})
			->groupBy('year')
			->groupBy('nm_kecamatan')
			->groupBy('nm_kelurahan')
			->orderBy('year', 'ASC')
			->get();

		$data = [];
		foreach ($sppt as $row) {
			$find = $query->where('year', $row->year)->where('nm_kecamatan', $row->nm_kecamatan)
				->where('nm_kelurahan', $row->nm_kelurahan)->first();

			$stts = 0;
			$pbb = 0;
			$denda = 0;
			if ($find) {
				$stts = $find->stts;
				$pbb = $find->pbb;
				$denda = $find->denda;
			}

			$data[] = [
				'year' => $row->year,
				'stts_ketetapan' => $row->stts_ketetapan,
				'ketetapan' => $row->ketetapan,
				'nm_kecamatan' => $row->nm_kecamatan,
				'nm_kelurahan' => $row->nm_kelurahan,
				'stts' => $stts,
				'pbb' => $pbb - $denda,
				'denda' => $denda
			];
		}
		$data = collect($data)->groupBy('nm_kecamatan')->map(function($item, $index) {
			$data = [
				'stts_ketetapan' => $item->sum('stts_ketetapan'),
				'ketetapan' => $item->sum('ketetapan'),
				'stts' => $item->sum('stts'),
				'pbb' => $item->sum('pbb'),
				'denda' => $item->sum('denda'),
			];
			$kelurahan = array_unique($item->pluck('nm_kelurahan')->values()->all());
			return [
				'nm_kecamatan' => $index,
				'items' => $data,
				'kelurahan' => $kelurahan,
				'data' => $item
			];
		})->values()->toArray();
		
		$stts_ketetapan = collect($data)->sum(function($item) {
			return $item['items']['stts_ketetapan'];
		});
		$ketetapan = collect($data)->sum(function($item) {
			return $item['items']['ketetapan'];
		});
		$stts = collect($data)->sum(function($item) {
			return $item['items']['stts'];
		});
		$pbb = collect($data)->sum(function($item) {
			return $item['items']['pbb'];
		});
		$denda = collect($data)->sum(function($item) {
			return $item['items']['denda'];
		});
		$meta = [
			'stts_ketetapan' => $stts_ketetapan,
			'ketetapan' => $ketetapan,
			'stts' => $stts,
			'pbb' => $pbb,
			'denda' => $denda
		];
		return [
			'data' => $data,
			'meta' => $meta
		];
	}

	public function reportPenerimaanBerjalanPdf()
	{
		$path = storage_path('app/public/report');
        if (!File::isDirectory($path)) {
            File::makeDirectory($path);
        }
	 
		$penerimaan = $this->queryPenerimaanBerjalan();

		$pdf = PDF::loadView('reports.penerimaan_berjalan', compact('penerimaan'))
			->setPaper('legal', 'landscape');
		$pdf->save($path . '/penerimaan_berjalan.pdf');
		
		// return response()->file(public_path('storage/report/penerimaan_berjalan.pdf'), ['Content-Type' => 'application/pdf']);
        return response()->json([
            'status' => 'success',
            'data' => asset('storage/report/penerimaan_berjalan.pdf')
        ]);
	}

	public function reportPenerimaanBerjalanExcel(Request $request)
	{
		$path = storage_path('app/public/report');
        if (!File::isDirectory($path)) {
            File::makeDirectory($path);
        }
	 
		$penerimaan = $this->queryPenerimaanBerjalan();

		Excel::store(new ReportPenerimaanBerjalan($penerimaan), 'public/report/penerimaan-berjalan.xlsx');
        return response()->json([
            'status' => 'success',
            'data' => asset('storage/report/penerimaan-berjalan.xlsx')
        ]);
	}
}
