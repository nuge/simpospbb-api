<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::orderBy('name', 'ASC');
        $roles = request()->type == 'all' ? $roles->get():$roles->paginate(10);
        return response()->json(['status' => 'success', 'data' => $roles]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100|unique:roles,name'
        ]);

        Role::create(['name' => $request->name]);
        return response()->json(['status' => 'success']);
    }

    public function destroy($id)
    {
        $role = Role::withCount(['user'])->find($id);
        if ($role->user_count > 0) {
            return response()->json(['status' => 'error', 'data' => 'Role Telah Digunakan']);
        }
        $role->delete();
        return response()->json(['status' => 'success']);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:150|unique:roles,name,' . $id
        ]);

        $role = Role::find($id);
        $role->update(['name' => $request->name]);
        return response()->json(['status' => 'success']);
    }
}
