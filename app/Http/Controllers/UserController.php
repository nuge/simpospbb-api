<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\TP;

class UserController extends Controller
{
    public function index()
    {
        $users = User::with(['role'])->orderBy('created_at', 'DESC')->paginate(10);
        return response()->json(['status' => 'success', 'data' => $users]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:150',
            'nip' => 'required|numeric',
            'nip_baru' => 'required|numeric',
            'tempat_pembayaran' => 'required|numeric|digits:10',
            'phone_number' => 'nullable|numeric',
            'username' => 'required|unique:users,username',
            'password' => 'required|string|min:6',
            'level' => 'required',
            'email' => 'required|email|unique:users,email'
        ]);

        $tempat_pembayaran = $request->tempat_pembayaran;
        $user = User::orderBy('id', 'DESC')->first();
        User::create([
            'id' => $user ? $user->id + 1:1,
            'name' => $request->name,
            'nip' => $request->nip,
            'nip_baru' => $request->nip_baru,
            'kd_kanwil' => $tempat_pembayaran[0] . $tempat_pembayaran[1],
            'kd_kppbb' => $tempat_pembayaran[2] . $tempat_pembayaran[3],
            'kd_bank_tunggal' => $tempat_pembayaran[4] . $tempat_pembayaran[5],
            'kd_bank_persepsi' => $tempat_pembayaran[6] . $tempat_pembayaran[7],
            'kd_tp' => $tempat_pembayaran[8] . $tempat_pembayaran[9],
            'telp' => $request->phone_number,
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role_id' => $request->level,
            'is_aktif' => 1,
            'bphtb_user_id' => 0,
            'bphtb_loket_id' => 0
        ]);
        return response()->json(['status' => 'success']);
    }

    public function changeStatus($id)
    {
        $user = User::find($id);
        $user->update(['is_aktif' => $user->is_aktif == 1 ? 0:1]);
        return response()->json(['status' => 'success']);
    }
}
