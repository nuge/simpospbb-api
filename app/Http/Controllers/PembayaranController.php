<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sppt;
use App\PembayaranSppt;
use App\LogPembatalanPembayaran;
use Carbon\Carbon;
use DB;
use PDF;

class PembayaranController extends Controller
{
    private function selectPembayaran($request, $q = null)
    {
        $status = $request->status_pembayaran_sppt ? $request->status_pembayaran_sppt:0;
        $thn_pajak_sppt = $request->thn_pajak_sppt ? $request->thn_pajak_sppt:NULL;
        $searchsppt = Sppt::invoice($request->kd_propinsi, $request->kd_dati2, $request->kd_kecamatan, $request->kd_kelurahan, $request->kd_blok, $request->no_urut, $request->kd_jns_op, $thn_pajak_sppt, $status)
            ->with('ref_kecamatan', 'ref_kelurahan');	
    	return $searchsppt;
    }

    public function findData(Request $request)
    {
    	$this->validate($request, [
    		'kd_propinsi'	=> 'required|max:2',
    		'kd_dati2'	=> 'required|max:2',
    		'kd_kecamatan'	=> 'required|max:3',
    		'kd_kelurahan'	=> 'required|max:3',
    		'kd_blok'	=> 'required|max:3',
    		'no_urut'	=> 'required|max:4',
    		'kd_jns_op'	=> 'required|max:1',
    		'thn_pajak_sppt'	=> 'nullable|max:4'			
        ]);
        
        try {
            $selectSppt = $this->selectPembayaran($request)->get();
            
            $total = 0;
            $data = [];
            $data_tagihan = [];
            foreach ($selectSppt as $row) {
                $jumlah = $row->pbb_yg_harus_dibayar_sppt + $row->denda();
                $total += $jumlah;

                $data_tagihan = [
                    'nm_wp_sppt'	=> $row->nm_wp_sppt,
                    'jln_wp_sppt'	=> $row->jln_wp_sppt,
                    'kd_blok'		=> $row->kd_blok,
                    'no_urut'		=> $row->no_urut,
                    'kelurahan_wp_sppt'	=> $row->kelurahan_wp_sppt,
                    'kota_wp_sppt'	=> $row->kota_wp_sppt,
                    'nm_kelurahan'	=> $row->ref_kelurahan ? $row->ref_kelurahan->nm_kelurahan:'',
                    'nm_kecamatan'	=> $row->ref_kecamatan ? $row->ref_kecamatan->nm_kecamatan:'',
                    'total' => number_format($total)
                ];
                $data[] = [
                    'id'    => $row->identitas,
                    'thn_pajak_sppt' => $row->thn_pajak_sppt,
                    'pbb_yg_harus_dibayar_sppt' => number_format($row->pbb_yg_harus_dibayar_sppt),
                    'denda' => number_format($row->denda()),
                    'jumlah' => number_format($jumlah),
                    'tgl_jatuh_tempo_sppt' => $row->tgl_jatuh_tempo_sppt->format('d-m-Y'),
                    'status_pembayaran_sppt' => $row->status_pembayaran_sppt
                ];
            }
            $sppt = [
                'status' => 'success',
                'data_tagihan' => $data_tagihan,
                'data' => $data
            ];
        } catch (\Exception $e) {
            $sppt = [
                'status' => 400,
                'data' => $e->getMessage()
            ];
        }
    	return response()->json($sppt);
    }

    public function simpanPembayaran(Request $request)
    {
        $this->validate($request, [
    		'selected'	=> 'required'
        ]);

        $failedData = [];
        foreach ($request->selected as $row) {
            $nop = ExplodeNOP($row['id']);
            $searchsppt = Sppt::pembayaransppt($nop['kd_propinsi'], $nop['kd_dati2'], $nop['kd_kecamatan'], $nop['kd_kelurahan'], $nop['kd_blok'], $nop['no_urut'], $nop['kd_jns_op'], $nop['thn_pajak_sppt']);

            if ($searchsppt->get()->count() > 0) {
                $selectSppt = $searchsppt->first();
                $denda = $selectSppt->denda();

                //FILTER OPTIONS
                $pospbb_options = FilterPayment();

                //FILTER DAY
                $day = Carbon::now()->format('D');
                $time = Carbon::now()->format('H:i:s');
                if ($day == $pospbb_options['end_day']) {
                    $tgl_pembayaran_sppt = Carbon::now()->format('Y-m-d');
                    if ($time > $pospbb_options['end_time']) {
                        $tgl_pembayaran_sppt = Carbon::now()->addDays($pospbb_options['add_day'])->format('Y-m-d');
                    }
                } else {
                    $tgl_pembayaran_sppt = Carbon::now()->format('Y-m-d');
                    if ($time > $pospbb_options['end_time']) {
                        $tgl_pembayaran_sppt = Carbon::now()->addDays(1)->format('Y-m-d');
                    }
                }

                //FILTER TRANSACTION
                if (empty($selectSppt->denda_sppt) && empty($selectSppt->jml_sppt_yg_dibayar) && empty($selectSppt->tgl_pembayaran_sppt) && empty($selectSppt->tgl_rekam_byr_sppt) && empty($selectSppt->tgl_rekam_byr_sppt) && empty($selectSppt->nip_rekam_byr_sppt)) {
                    DB::beginTransaction();
                    try {
                        $user = request()->user();
                        $pembayaran_sppt = PembayaranSppt::create([
                            'kd_propinsi' => $selectSppt->kd_propinsi,
                            'kd_dati2' => $selectSppt->kd_dati2,
                            'kd_kecamatan' => $selectSppt->kd_kecamatan,
                            'kd_kelurahan' => $selectSppt->kd_kelurahan,
                            'kd_blok' => $selectSppt->kd_blok,
                            'no_urut' => $selectSppt->no_urut,
                            'kd_jns_op' => $selectSppt->kd_jns_op,
                            'thn_pajak_sppt' => $selectSppt->thn_pajak_sppt,
                            'pembayaran_sppt_ke' => 1,
                            'kd_kanwil_bank' => $selectSppt->kd_kanwil_bank,
                            'kd_kppbb_bank' => $selectSppt->kd_kppbb_bank,
                            'kd_bank_tunggal' => $selectSppt->kd_bank_tunggal,
                            'kd_bank_persepsi' => $selectSppt->kd_bank_persepsi,
                            'kd_tp' => $selectSppt->kd_tp,
                            'denda_sppt' => $denda,
                            'jml_sppt_yg_dibayar' => $selectSppt->pbb_yg_harus_dibayar_sppt + $denda,
                            'tgl_pembayaran_sppt' => $tgl_pembayaran_sppt,
                            'tgl_rekam_byr_sppt'  => Carbon::now()->format('Y-m-d'),
                            'nip_rekam_byr_sppt'  => $user->nip
                        ]);

                        $selectSppt->update(['status_pembayaran_sppt' => 1]);
                        DB::commit();
                    } catch (\Exception $e) {
                        DB::rollback();
                        $failedData[] = [
                            'id' => $row['id'],
                            'message' => $e->getMessage()
                        ];
                    }
                } else {
                    $selectSppt->update(['status_pembayaran_sppt' => 1]);
                }
            } else {
                $failedData[] = ['id' => $row['id'], 'message' => 'No Data'];
            }
        }
        return response()->json(['status' => 'success', 'data' => $failedData]);
    }

    public function cetakBuktiPembayaran(Request $request)
    {
        $pembayaran_sppt = [];
		foreach ($request->selected as $i) {
			$nop = ExplodeNOP($i['id']);
			$result = PembayaranSppt::bill3(
				$nop['kd_propinsi'], 
				$nop['kd_dati2'], 
				$nop['kd_kecamatan'], 
				$nop['kd_kelurahan'], 
				$nop['kd_blok'], 
				$nop['no_urut'], 
				$nop['kd_jns_op'], 
				$nop['thn_pajak_sppt']
			)->get();
			if (count($result) > 0) $pembayaran_sppt[] = $result;
		}

        $paddingLeft = [10+26 . 'mm', 10+42 . 'mm', 10+25 . 'mm', 10+30 . 'mm', 10+5 . 'mm'];
        $paddingTop = ['10px', '30px', '70px'];
        $pdf = PDF::loadView('reports.pembayaran', compact('paddingLeft', 'paddingTop', 'pembayaran_sppt'))
            ->setPaper('a4', 'potrait');
        // return $pdf->stream();
        $pdf->save(storage_path('app/public/bukti_bayar.pdf'));
        // return response()->file(public_path('storage/bukti_bayar.pdf'), ['Content-Type' => 'application/pdf']);
        return response()->json([
            'status' => 'success',
            'data' => route('report.pdf', ['filename' => 'bukti_bayar.pdf'])
        ]);
    }

    //KOLEKTIF
    public function kolektif(Request $request)
    {
        $this->validate($request, [
            'kd_kecamatan' => 'required|max:3',
            'kd_kelurahan' => 'required|max:3',
            'kd_blok' => 'required|max:3',
            'thn_pajak_sppt' => 'required|max:4',
            'q' => 'nullable|string'
        ]);
        
		$sppt = $this->queryKolektif($request);
        return response()->json(['status' => 'success', 'data' => $sppt['data'], 'total_data' => $sppt['total_data']]);
    }

    private function queryKolektif($request) 
	{
        $sppt = Sppt::where('kd_kecamatan', $request->kd_kecamatan)
            ->where('kd_kelurahan', $request->kd_kelurahan)
            ->where('kd_blok', $request->kd_blok)
            ->where('thn_pajak_sppt', $request->thn_pajak_sppt)
            ->where('status_pembayaran_sppt', 0)
            ->orderBy('thn_pajak_sppt', 'ASC')
            ->orderBy('kd_jns_op', 'ASC') 
            ->orderBy('no_urut', 'ASC') 
            ->orderBy('kd_blok', 'ASC')
            ->orderBy('kd_kelurahan', 'ASC') 
            ->orderBy('kd_kecamatan', 'ASC') 
            ->orderBy('kd_dati2', 'ASC') 
            ->orderBy('kd_propinsi', 'ASC')->get();
    
        $pbb_pokok = 0;
        $denda = 0;
        $jumlah = 0;

        $total_data = [];
        $data = [];
        foreach ($sppt as $value) {
            $pbb_pokok += $value->pbb_yg_harus_dibayar_sppt;
            $denda += $value->denda();
            $jumlah += $pbb_pokok + $denda;

            $data[] = [
                'id' => $value->identitas,
                // 'nop' => $value->identitas,
                'nop' => substr($value->identitas, 0, -4),
                'nm_wp_sppt' => $value->nm_wp_sppt,
                'jln_wp_sppt' => $value->jln_wp_sppt . ' ' . $value->kelurahan_wp_sppt . ' ' . $value->kota_wp_sppt,
                'tgl_jatuh_tempo_sppt' => $value->tgl_jatuh_tempo_sppt->format('d-m-Y'),
                'pbb_yg_harus_dibayar_sppt' => number_format($value->pbb_yg_harus_dibayar_sppt),
                'denda' => number_format($value->denda()),
                'jumlah' => number_format($value->pbb_yg_harus_dibayar_sppt + $value->denda()),
                'status_pembayaran_sppt' => $value->status_pembayaran_sppt
            ];

            $total_data = [
                'pbb_pokok' => number_format($pbb_pokok),
                'denda' => number_format($denda),
                'jumlah' => number_format($jumlah)
            ];
        }
		return [
            'total_data' => $total_data,
            'data' => $data
        ];
    }
    
    public function pembatalanPembayaran(Request $request)
    {
    	$this->validate($request, [
    		'kd_propinsi'	=> 'required|max:2',
    		'kd_dati2'	=> 'required|max:2',
    		'kd_kecamatan'	=> 'required|max:3',
    		'kd_kelurahan'	=> 'required|max:3',
    		'kd_blok'	=> 'required|max:3',
    		'no_urut'	=> 'required|max:4',
    		'kd_jns_op'	=> 'required|max:1'
        ]);
        
        try {
			$result = PembayaranSppt::with(['kelurahan', 'kecamatan'])->bill($request->kd_propinsi, $request->kd_dati2, $request->kd_kecamatan, $request->kd_kelurahan, $request->kd_blok, $request->no_urut, $request->kd_jns_op)
                ->get();
            
            $total = 0;
            $data = [];
            $data_tagihan = [];
            foreach ($result as $row) {
                $jumlah = $row->jml_sppt_yg_dibayar;
                $total += $jumlah;

                $data_tagihan = [
                    'nm_wp_sppt'	=> $row->nm_wp_sppt,
                    'jln_wp_sppt'	=> $row->jln_wp_sppt,
                    'kelurahan_wp_sppt'	=> $row->kelurahan_wp_sppt,
                    'kota_wp_sppt'	=> $row->kota_wp_sppt,
                    'nm_kelurahan'	=> $row->kelurahan->nm_kelurahan,
                    'nm_kecamatan'	=> $row->kecamatan->nm_kecamatan,
                    'total' => number_format($total)
                ];
                $data[] = [
                    'id'    => $row->identitas,
                    'thn_pajak_sppt' => $row->thn_pajak_sppt,
                    'tgl_pembayaran' => $row->tgl_pembayaran_sppt->format('d-m-Y'),
                    'pbb_yg_harus_dibayar_sppt' => number_format($row->pbb_yg_harus_dibayar_sppt),
                    'denda' => number_format($row->denda_sppt),
                    'jumlah' => number_format($jumlah),
                    'tgl_jatuh_tempo_sppt' => $row->tgl_jatuh_tempo_sppt->format('d-m-Y'),
                    'pembayaran_sppt_ke' => $row->pembayaran_sppt_ke,
                    'status_pembayaran_sppt' => $row->status_pembayaran_sppt
                ];
            }
            return response()->json(['status' => 'success', 'data_tagihan' => $data_tagihan, 'data' => $data]);
        } catch (\Exception $e) {
            $result = [
                'status' => 'error',
                'data' => $e->getMessage()
            ];
            return response()->json($result);
        }
    }

    public function prosesPembatalan(Request $request)
    {
        $this->validate($request, [
    		'selected'	 => 'required',
			'alasan' => 'required'
        ]);
		
		DB::beginTransaction();
        try {
			foreach ($request->selected as $i) {
				$nop = ExplodeNOP($i['id']);
				$pembayaran = PembayaranSppt::where('kd_propinsi', $nop['kd_propinsi'])
					->where('kd_dati2', $nop['kd_dati2'])
					->where('kd_kecamatan', $nop['kd_kecamatan'])
					->where('kd_kelurahan', $nop['kd_kelurahan'])
					->where('kd_blok', $nop['kd_blok'])
					->where('no_urut', $nop['no_urut'])
					->where('kd_jns_op', $nop['kd_jns_op'])
					->where('thn_pajak_sppt', $nop['thn_pajak_sppt']);
					
                $p = $pembayaran->first();
                $user = request()->user();
                $logpembatalan = LogPembatalanPembayaran::create([
					'kd_propinsi'   		=> $p->kd_propinsi,
					'kd_dati2'  			=> $p->kd_dati2,
					'kd_kecamatan'  		=> $p->kd_kecamatan,
					'kd_kelurahan'  		=> $p->kd_kelurahan,
					'kd_blok'   			=> $p->kd_blok,
					'no_urut'   			=> $p->no_urut,
					'kd_jns_op' 			=> $p->kd_jns_op,
					'thn_pajak_sppt'    	=> $p->thn_pajak_sppt,
					'pembayaran_sppt_ke'    => $p->pembayaran_sppt_ke,
					'kd_kanwil_bank'    	=> $p->kd_kanwil_bank,
					'kd_kppbb_bank' 		=> $p->kd_kppbb_bank,
					'kd_bank_tunggal'   	=> $p->kd_bank_tunggal,
					'kd_bank_persepsi'  	=> $p->kd_bank_persepsi,
					'kd_tp' 				=> $p->kd_tp,
					'denda_sppt'    		=> $p->denda_sppt,
					'jml_sppt_yg_dibayar'   => $p->jml_sppt_yg_dibayar,
					'tgl_pembayaran_sppt'   => $p->tgl_pembayaran_sppt,
					'tgl_rekam_btl_sppt'    => Carbon::now()->format('Y-m-d'),
					'nip_rekam_btl_sppt'    => $user->nip,
					'ip'    				=> request()->ip(),
					'alasan'				=> $request->alasan
				]);
				$pembayaran->delete();

				$cancelsppt = Sppt::where('kd_propinsi', $nop['kd_propinsi'])
					->where('kd_dati2', $nop['kd_dati2'])
					->where('kd_kecamatan', $nop['kd_kecamatan'])
					->where('kd_kelurahan', $nop['kd_kelurahan'])
					->where('kd_blok', $nop['kd_blok'])
					->where('no_urut', $nop['no_urut'])
					->where('kd_jns_op', $nop['kd_jns_op'])
                    ->where('thn_pajak_sppt', $nop['thn_pajak_sppt'])
                    ->first();
				$cancelsppt->update(['STATUS_PEMBAYARAN_SPPT' => 0]);
			}
			DB::commit();
            return response()->json(['status' => 'success']);
		} catch (\Exception $e) {
			DB::rollback();
			return response()->json(['status' => 'error', 'data' => $e->getMessage()], 500);
		}
    }

    public function findDataKurangBayar(Request $request)
    {
        $this->validate($request, [
            'nop' => 'required|string|max:24|min:24',
            'tahun' => 'required|numeric|digits:4'
        ]);

        $nop = explode('.', $request->nop);
        $sppt = Sppt::where(function($q) use($nop) {
            $q->where('kd_propinsi', $nop[0])
            ->where('kd_dati2', $nop[1])
            ->where('kd_kecamatan', $nop[2])
            ->where('kd_kelurahan', $nop[3])
            ->where('kd_blok', $nop[4])
            ->where('no_urut', $nop[5])
            ->where('kd_jns_op', $nop[6])
            ->where('thn_pajak_sppt', request()->tahun);
        })->first();

        $tlh_dibayar = PembayaranSppt::selectRaw('coalesce(SUM(jml_sppt_yg_dibayar), 0) as total, coalesce(SUM(denda_sppt), 0) as total_denda')
            ->where(function($q) use($nop) {
            $q->where('kd_propinsi', $nop[0])
                ->where('kd_dati2', $nop[1])
                ->where('kd_kecamatan', $nop[2])
                ->where('kd_kelurahan', $nop[3])
                ->where('kd_blok', $nop[4])
                ->where('no_urut', $nop[5])
                ->where('kd_jns_op', $nop[6])
                ->where('thn_pajak_sppt', request()->tahun);
        })->get();
        $pembayaran = PembayaranSppt::where(function($q) use($nop) {
            $q->where('kd_propinsi', $nop[0])
            ->where('kd_dati2', $nop[1])
            ->where('kd_kecamatan', $nop[2])
            ->where('kd_kelurahan', $nop[3])
            ->where('kd_blok', $nop[4])
            ->where('no_urut', $nop[5])
            ->where('kd_jns_op', $nop[6])
            ->where('thn_pajak_sppt', request()->tahun);
        })->orderBy('pembayaran_sppt_ke', 'DESC')->first();

        if ($sppt && $pembayaran) {
            if ($sppt->pbb_yg_harus_dibayar_sppt > $tlh_dibayar[0]->total) {
                $total_pokok = $sppt->pbb_yg_harus_dibayar_sppt + $sppt->denda();
                $data = [
                    'name' => $sppt->nm_wp_sppt,
                    'address' => $sppt->jln_wp_sppt . ' ' . $sppt->kelurahan_wp_sppt . ' - ' . $sppt->kota_wp_sppt,
                    'address_op' => null,
                    'due_date' => $sppt->tgl_jatuh_tempo_sppt->format('Y-m-d'),
                    'pbb_pokok' => number_format($sppt->pbb_yg_harus_dibayar_sppt),
                    'pembayaran_ke' => $pembayaran->pembayaran_sppt_ke + 1,
                    'pembayaran_pokok' => number_format($tlh_dibayar[0]->total - $tlh_dibayar[0]->total_denda),
                    'denda' => number_format($sppt->denda()),
                    'pembayaran_denda' => number_format($tlh_dibayar[0]->total_denda),
                    'total_pokok' => number_format($total_pokok),
                    'jumlah_dibayar' => number_format($tlh_dibayar[0]->total),
                    'sisa' => number_format($total_pokok - $tlh_dibayar[0]->total)
                ];
                return response()->json(['status' => 'success', 'data' => $data]);
            }
            return response()->json(['status' => 'error', 'data' => 'Tidak ada data']);
        }
        return response()->json(['status' => 'error', 'data' => 'Tidak ada data']);
    }

    public function storePembayaranKurangBayar(Request $request)
    {
        $this->validate($request, [
            'nop' => 'required|string|max:24|min:24',
            'tahun' => 'required|numeric|digits:4',
            'pembayaran_sppt_ke' => 'required|integer'
        ]);

        DB::beginTransaction();
        try {
            $user = $request->user();
            $nop = explode('.', $request->nop);
            $sppt = Sppt::where(function($q) use($nop) {
                $q->where('kd_propinsi', $nop[0])
                ->where('kd_dati2', $nop[1])
                ->where('kd_kecamatan', $nop[2])
                ->where('kd_kelurahan', $nop[3])
                ->where('kd_blok', $nop[4])
                ->where('no_urut', $nop[5])
                ->where('kd_jns_op', $nop[6])
                ->where('thn_pajak_sppt', request()->tahun);
            })->first();
            if ($sppt) {
                $tlh_dibayar = PembayaranSppt::selectRaw('coalesce(SUM(jml_sppt_yg_dibayar), 0) as total, coalesce(SUM(denda_sppt), 0) as total_denda')
                    ->where(function($q) use($nop) {
                    $q->where('kd_propinsi', $nop[0])
                        ->where('kd_dati2', $nop[1])
                        ->where('kd_kecamatan', $nop[2])
                        ->where('kd_kelurahan', $nop[3])
                        ->where('kd_blok', $nop[4])
                        ->where('no_urut', $nop[5])
                        ->where('kd_jns_op', $nop[6])
                        ->where('thn_pajak_sppt', request()->tahun);
                })->get();

                $denda = $sppt->denda() - $tlh_dibayar[0]->total_denda;
                $jml_yg_dibayar = $sppt->pbb_yg_harus_dibayar_sppt - $tlh_dibayar[0]->total;

                $pbs = PembayaranSppt::orderBy('id', 'DESC')->first();
                PembayaranSppt::create([
                    'kd_propinsi' => $sppt->kd_propinsi,
                    'kd_dati2' => $sppt->kd_dati2,
                    'kd_kecamatan' => $sppt->kd_kecamatan,
                    'kd_kelurahan' => $sppt->kd_kelurahan,
                    'kd_blok' => $sppt->kd_blok,
                    'no_urut' => $sppt->no_urut,
                    'kd_jns_op' => $sppt->kd_jns_op,
                    'thn_pajak_sppt' => $sppt->thn_pajak_sppt,
                    'pembayaran_sppt_ke' => $request->pembayaran_sppt_ke,
                    'kd_kanwil_bank' => $user->kd_kanwil,
                    'kd_kppbb_bank' => $user->kd_kppbb,
                    'kd_bank_tunggal' => $user->kd_bank_tunggal,
                    'kd_bank_persepsi' => $user->kd_bank_persepsi,
                    'kd_tp' => $user->kd_tp,
                    'denda_sppt' => $denda,
                    'jml_sppt_yg_dibayar' => $jml_yg_dibayar,
                    'tgl_pembayaran_sppt' => Carbon::now()->format('Y-m-d'),
                    'tgl_rekam_byr_sppt' => Carbon::now()->format('Y-m-d'),
                    'nip_rekam_byr_sppt' => $user->nip,
                    'id' => $pbs ? $pbs->id:1,
                    'biaya_admin' => 0
                ]);
                $sppt->update(['status_pembayaran_sppt' => 1]);

                DB::commit();
                return response()->json(['status' => 'success']);
            }
            return response()->json(['status' => 'error', 'data' => 'Data Tidak Ditemukan']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 'error', 'data' => $e->getMessage()]);
        }
    }
}