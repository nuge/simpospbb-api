<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth as Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|exists:users,email',
            'password'  => 'required|min:6'
        ]);
		
		// $credentials = $request->only('email', 'password');
		$user = null;
        
		// if ($token = Auth::guard('api')->setTTL(Auth::guard('api')->factory()->getTTL() * 360)->attempt($credentials)) { 
        $user = User::where('email', $request->email)->first();
        if($user->is_aktif == 0) return response()->json(['status' => 'error', 'data' => 'Akun Anda Tidak Aktif!']);
        
        if (!captcha_api_check($request->captcha, $request->keyCaptcha)) {
            return response()->json(['status' => 'error', 'data' => 'Captcha Salah!']);
        }

		if (Hash::check($request->password, $user->password)) { 
            // DB::beginTransaction();
            try {
                $token = Str::random(40);
                DB::table('users')->where('EMAIL', $request->email)->update([
                    'is_aktif' => 1,
                    'api_token' => $token,
                    'user_agent'    => $request->server('HTTP_USER_AGENT') . ' IP Address: ' . $request->ip()
                ]);
                // $user->logAuth($user->username, $request->ip(), 'LOGIN');
                // DB::commit();
                return response()->json(['status' => 'success', 'data' => [
                    'user' => [
						'email' => $request->email,					
						'token' => $token,
						// 'expires_in' => Auth::guard('api')->factory()->getTTL(),
						'token_type' => 'bearer',
						'username' => $user->username,
					]
                ]]);
            } catch (\Exception $e) {
                // DB::rollback();
                $success = false;
				return response()->json(['status' => 'error', 'data' => $e->getMessage()]);
            }
        } 
		return response()->json(['status' => 'error', 'data' => 'Email/Password Salah!']);
    }

    public function logout()
    {
        // Auth::guard('api')->logout();
        $user = request()->user();
        $user->update(['api_token' => null]);
        return response()->json(['status' => 'success']);
    }

    public function getCaptchaImage()
    {
        $captcha = \Captcha::create('flat', true);
        return response()->json(['status' => 'success', 'data' => $captcha]);
    }
}
