<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;
use App\PembayaranSppt;
use App\Sppt;

class HomeController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function summaryDashboard()
    {
        $start = Carbon::now()->format('Y-m') . '-01 00:00:01';
        $startToday = Carbon::now()->format('Y-m-d') . ' 00:00:01';
        $endToday = Carbon::now()->format('Y-m-d') . ' 23:59:59';

        $user = User::where('is_aktif', 1)->count();
        $transaksi = PembayaranSppt::whereBetween('tgl_pembayaran_sppt', [$startToday, $endToday])->count();
        $penerimaan = PembayaranSppt::selectRaw('coalesce(SUM(jml_sppt_yg_dibayar), 0) as total')
            ->whereBetween('tgl_pembayaran_sppt', [$startToday, $endToday])->get();
        $penerimaanUntilToday = PembayaranSppt::selectRaw('coalesce(SUM(jml_sppt_yg_dibayar), 0) as total')
            ->whereBetween('tgl_pembayaran_sppt', [$start, $endToday])->get();

        return response()->json(['status' => 'success', 'data' => [
            'transaksi' => number_format($transaksi),
            'penerimaan' => number_format($penerimaan[0]->total),
            'penerimaan_until_today' => number_format($penerimaanUntilToday[0]->total),
            'user_online' => number_format($user)
        ]]);
    }

    public function summaryChart()
    {
        $yearActive = request()->year;
        $previousYear = $yearActive - 1;

        $activeMonth = [
            ['month' => 1, 'active' => 0, 'previous' => 0],
            ['month' => 2, 'active' => 0, 'previous' => 0],
            ['month' => 3, 'active' => 0, 'previous' => 0],
            ['month' => 4, 'active' => 0, 'previous' => 0],
            ['month' => 5, 'active' => 0, 'previous' => 0],
            ['month' => 6, 'active' => 0, 'previous' => 0],
            ['month' => 7, 'active' => 0, 'previous' => 0],
            ['month' => 8, 'active' => 0, 'previous' => 0],
            ['month' => 9, 'active' => 0, 'previous' => 0],
            ['month' => 10, 'active' => 0, 'previous' => 0],
            ['month' => 11, 'active' => 0, 'previous' => 0],
            ['month' => 12, 'active' => 0, 'previous' => 0]
        ];

        //ACTIVE MONTH
        $active = PembayaranSppt::selectRaw('Extract(MONTH from tgl_pembayaran_sppt) as month, coalesce(SUM(jml_sppt_yg_dibayar), 0) as total')
            ->whereYear('tgl_pembayaran_sppt', $yearActive)
            ->groupBy('month')
            ->orderBy('month', 'ASC')
            ->get();

        foreach ($active as $row) {
            $key = array_search($row->month, array_column($activeMonth, 'month'));
            if ($key != -1) {
                $activeMonth[$key]['active'] = $row->total;
            }
        }

        //PREVIOUS MONTH
        $previous = PembayaranSppt::selectRaw('Extract(MONTH from tgl_pembayaran_sppt) as month, coalesce(SUM(jml_sppt_yg_dibayar), 0) as total')
            ->whereYear('tgl_pembayaran_sppt', $previousYear)
            ->groupBy('month')
            ->orderBy('month', 'ASC')
            ->get();

        foreach ($previous as $row) {
            $key = array_search($row->month, array_column($activeMonth, 'month'));
            if ($key != -1) {
                $activeMonth[$key]['previous'] = $row->total;
            }
        }
        
        $transaction = PembayaranSppt::whereYear('tgl_pembayaran_sppt', $yearActive)->count();
        return response()->json(['status' => 'success', 'data' => [
            'active' => collect($activeMonth)->pluck('active')->all(),
            'previous' => collect($activeMonth)->pluck('previous')->all(),
            'transaction' => $transaction
        ]]);
    }
}
