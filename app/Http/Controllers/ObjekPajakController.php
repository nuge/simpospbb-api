<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DatObjekPajak;
use App\DatSubjekPajak;

class ObjekPajakController extends Controller
{
    //kd_propinsi=73&kd_dati2=05&kd_kecamatan=010&kd_kelurahan=001&kd_blok=001&no_urut=0001&kd_jns_op=0
    public function search(Request $request)
    {
        $this->validate($request, [
            'kd_propinsi' => 'required',
            'kd_dati2' => 'required',
            'kd_kecamatan' => 'required',
            'kd_kelurahan' => 'required',
            'kd_blok' => 'required',
            'no_urut' => 'required',
            'kd_jns_op' => 'required',
        ]);

        $kd_propinsi = $request->kd_propinsi;
        $kd_dati2 = $request->kd_dati2;
        $kd_kecamatan = $request->kd_kecamatan;
        $kd_kelurahan = $request->kd_kelurahan;
        $kd_blok = $request->kd_blok;
        $no_urut = $request->no_urut;
        $kd_jns_op = $request->kd_jns_op;
        $nm_wp = $request->nm_wp;
        $alamat = $request->alamat;
        $q = $request->q;

        try {
            if ($request->type == 'nop') {
                $getDsp = DatObjekPajak::select('dat_objek_pajak.*', 'dat_subjek_pajak.nm_wp', 'dat_subjek_pajak.jalan_wp', 'dat_subjek_pajak.blok_kav_no_wp', 'dat_subjek_pajak.rw_wp', 'dat_subjek_pajak.rt_wp', 'dat_subjek_pajak.kelurahan_wp', 'dat_subjek_pajak.kota_wp', 'dat_subjek_pajak.kd_pos_wp', 'dat_subjek_pajak.telp_wp', 'dat_subjek_pajak.npwp')
                    ->nop($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, $q)
                    ->leftJoin('dat_subjek_pajak', 'dat_subjek_pajak.subjek_pajak_id', 'dat_objek_pajak.subjek_pajak_id')
                    ->paginate(10);
                return response()->json(['status' => 'success', 'data' => $getDsp]);
            }
            $getDsp = DatSubjekPajak::select('dat_subjek_pajak.*', 'dop.kd_propinsi', 'dop.kd_dati2', 'dop.kd_kecamatan', 'dop.kd_kelurahan', 'dop.kd_blok', 'dop.no_urut', 'dop.kd_jns_op', 'dop.jalan_op', 'dop.blok_kav_no_op', 'dop.rt_op', 'dop.rw_op')
                ->wherelike($nm_wp, $alamat, $q)
                ->leftJoin('dat_objek_pajak as dop','dop.subjek_pajak_id', 'dat_subjek_pajak.subjek_pajak_id')
                ->paginate(10);
            return response()->json(['status' => 'success', 'data' => $getDsp]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
