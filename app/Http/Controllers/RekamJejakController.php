<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use File;
use PDF;

class RekamJejakController extends Controller
{
    public function index(Request $request)
    {
        $informasiPajak = $this->informasiPajak($request);
        return response()->json(['status' => 'success', 'data' => [
            'wajib_pajak' => $this->wajibPajak($request),
            'pajak' => $informasiPajak['data'],
            'total' => $informasiPajak['total'],
        ]]);
    }

    private function informasiPajak($request)
    {
        $infoPajak = DB::table("sppt")
            ->leftJoin("pembayaran_sppt", function($join) {
                $join->on("pembayaran_sppt.kd_propinsi", "=", "sppt.kd_propinsi");
                $join->on("pembayaran_sppt.kd_dati2", "=", "sppt.kd_dati2");
                $join->on("pembayaran_sppt.kd_kecamatan", "=", "sppt.kd_kecamatan");
                $join->on("pembayaran_sppt.kd_kelurahan", "=", "sppt.kd_kelurahan");
                $join->on("pembayaran_sppt.kd_blok", "=", "sppt.kd_blok");
                $join->on("pembayaran_sppt.no_urut", "=", "sppt.no_urut");
                $join->on("pembayaran_sppt.kd_jns_op", "=", "sppt.kd_jns_op");                    
            })
            ->where('sppt.kd_propinsi', $request->kd_propinsi)
            ->where('sppt.kd_dati2', $request->kd_dati2)
            ->where('sppt.kd_kecamatan', $request->kd_kecamatan)
            ->where('sppt.kd_kelurahan', $request->kd_kelurahan)
            ->where('sppt.kd_blok', $request->kd_blok)
            ->where('sppt.no_urut', $request->no_urut)
            ->where('sppt.kd_jns_op', $request->kd_jns_op)
            ->where('pembayaran_sppt.pembayaran_sppt_ke', 1)
            ->orderBy('sppt.thn_pajak_sppt', 'DESC')
            ->orderBy('pembayaran_sppt.tgl_pembayaran_sppt', 'ASC')
            ->select(
                'sppt.thn_pajak_sppt',
                'sppt.luas_bumi_sppt',
                'sppt.luas_bng_sppt',
                'sppt.pbb_yg_harus_dibayar_sppt',
                'sppt.tgl_jatuh_tempo_sppt',
                'sppt.status_pembayaran_sppt',
                'pembayaran_sppt.jml_sppt_yg_dibayar',
                'pembayaran_sppt.tgl_pembayaran_sppt',
                'pembayaran_sppt.denda_sppt'
            )->get();
                
        $data = [];
        $total_harus_dibayar = 0;
        $total_sppt_yg_dibayar = 0;
        foreach ($infoPajak as $value) {
            $index = array_search($value->thn_pajak_sppt, array_column($data, 'thn_pajak_sppt'));
            if ($index === false) {
                $jml_sppt_yg_dibayar = $value->jml_sppt_yg_dibayar ? $value->jml_sppt_yg_dibayar:0;
                $tgl_pembayaran_sppt = $value->tgl_pembayaran_sppt ? $value->tgl_pembayaran_sppt:'-';
                
                // KURANG LEBIH BAYAR
                $krb_denda = $this->denda(date_format(date_create($value->tgl_jatuh_tempo_sppt), 'd-m-Y'), $value->pbb_yg_harus_dibayar_sppt);
                $krb_jml_harus_dibayar = $value->pbb_yg_harus_dibayar_sppt + $krb_denda;

                $denda = $value->denda_sppt;
                $kurleb_bayar = $jml_sppt_yg_dibayar - $krb_jml_harus_dibayar;
                if ($value->status_pembayaran_sppt  == 0) {
                    $denda = $krb_denda;
                    $kurleb_bayar = 0;
                    $total_harus_dibayar += $krb_jml_harus_dibayar;
                }

                $jml_harus_dibayar = $value->pbb_yg_harus_dibayar_sppt + $denda;
                $data[] = [
                    'thn_pajak_sppt' => $value->thn_pajak_sppt,
                    'luas_bumi_sppt' => $value->luas_bumi_sppt,
                    'luas_bng_sppt' => $value->luas_bng_sppt,
                    'pbb_yg_harus_dibayar_sppt' => number_format($value->pbb_yg_harus_dibayar_sppt),
                    'denda' => number_format($denda),
                    'jml_harus_dibayar' => number_format($jml_harus_dibayar),
                    'jml_sppt_yg_dibayar' => number_format($jml_sppt_yg_dibayar),
                    'kurleb_bayar' => number_format($kurleb_bayar),
                    'tgl_jatuh_tempo_sppt' => date_format(date_create($value->tgl_jatuh_tempo_sppt), 'd-m-Y'),
                    'tgl_pembayaran_sppt' => $tgl_pembayaran_sppt != '-' ? date_format(date_create($tgl_pembayaran_sppt), 'd-m-Y') : $tgl_pembayaran_sppt,
                    'status_pembayaran_sppt' => $value->status_pembayaran_sppt
                ];
                $total_sppt_yg_dibayar += $jml_sppt_yg_dibayar;       
            }
        }

        $sisa = $total_harus_dibayar - $total_sppt_yg_dibayar;
        $totals = [
            'total_harus_dibayar' => number_format($total_harus_dibayar),
            'total_sppt_yg_dibayar' => number_format($total_sppt_yg_dibayar),
            'total_sisa' =>  $sisa > 0 ? number_format($sisa):0
        ];
        return [
            'data' => $data,
            'total' => $totals
        ];
    }

    private function wajibPajak($request)
    {
        $wajibPajak = DB::table("sppt")
            ->join("ref_kelurahan", function($join) {
                $join->on("ref_kelurahan.kd_kelurahan", "=", "sppt.kd_kelurahan");
                $join->on("ref_kelurahan.kd_kecamatan", "=", "sppt.kd_kecamatan");
                $join->on("ref_kelurahan.kd_dati2", "=", "sppt.kd_dati2");
                $join->on("ref_kelurahan.kd_propinsi", "=", "sppt.kd_propinsi");					
            })
            ->join("ref_kecamatan", function($join) {
                $join->on("ref_kecamatan.kd_kecamatan", "=", "sppt.kd_kecamatan");
                $join->on("ref_kecamatan.kd_dati2", "=", "sppt.kd_dati2");
                $join->on("ref_kecamatan.kd_propinsi", "=", "sppt.kd_propinsi");					
            })
            ->join("ref_dati2", function($join) {
                $join->on("ref_dati2.kd_dati2", "=", "sppt.kd_dati2");
                $join->on("ref_dati2.kd_propinsi", "=", "sppt.kd_propinsi");					
            })
            ->join("ref_propinsi", function($join) {
                $join->on("ref_propinsi.kd_propinsi", "=", "sppt.kd_propinsi");					
            })
            ->where('sppt.kd_propinsi', $request->kd_propinsi)
            ->where('sppt.kd_dati2', $request->kd_dati2)
            ->where('sppt.kd_kecamatan', $request->kd_kecamatan)
            ->where('sppt.kd_kelurahan', $request->kd_kelurahan)
            ->where('sppt.kd_blok', $request->kd_blok)
            ->where('sppt.no_urut', $request->no_urut)
            ->where('sppt.kd_jns_op', $request->kd_jns_op)				
            ->select(
                'sppt.kd_propinsi', 
                'ref_propinsi.nm_propinsi',
                'sppt.kd_dati2', 
                'ref_dati2.nm_dati2',
                'sppt.kd_kecamatan', 
                'ref_kecamatan.nm_kecamatan',
                'sppt.kd_kelurahan',
                'ref_kelurahan.nm_kelurahan',
                'sppt.kd_blok',
                'sppt.no_urut',
                'sppt.kd_jns_op',
                'sppt.nm_wp_sppt',
                'sppt.jln_wp_sppt',
                'sppt.blok_kav_no_wp_sppt',
                'sppt.rw_wp_sppt',
                'sppt.rt_wp_sppt'
            )->distinct()->get();
		return $wajibPajak;	
    }

    private function denda($tgl, $value)
    {
		$jatuh_tempo = \Carbon\Carbon::createFromFormat('d-m-Y', $tgl, 'Asia/Makassar');
		$dateNow = \Carbon\Carbon::now();
		$diff = $jatuh_tempo->diffInMonths($dateNow);
		$diff_month = $diff > 24 ? 24:$diff;
		return $value * $diff_month * 0.02;
    }

    public function rekamJejakPdf(Request $request )
    {
        $path = storage_path('app/public/rekam-jejak');
        if (!File::isDirectory($path)) {
            File::makeDirectory($path);
        }
	 
        $informasiPajak = $this->informasiPajak($request);
        $wajibPajak = $this->wajibPajak($request);
        $nop = $request->kd_propinsi . $request->kd_dati2 . $request->kd_kecamatan . $request->kd_kelurahan . $request->kd_blok . $request->no_urut . $request->kd_jns_op;
        
		$pdf = PDF::loadView('reports.rekam_jejak', compact('informasiPajak', 'wajibPajak', 'nop'))
			->setPaper('legal', 'landscape');
		$pdf->save($path . '/rekam_jejak.pdf');
		
		// return response()->file(public_path('storage/rekam-jejak/rekam_jejak.pdf'), ['Content-Type' => 'application/pdf']);
        return response()->json([
            'status' => 'success',
            'data' => asset('storage/rekam-jejak/rekam_jejak.pdf')
        ]);
    }
}
