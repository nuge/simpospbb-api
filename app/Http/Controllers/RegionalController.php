<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RefKecamatan;
use App\RefKelurahan;

class RegionalController extends Controller
{
    public function getKecamatan()
    {
        $kecamatan = RefKecamatan::orderBy('nm_kecamatan', 'ASC')->get();
        return response()->json(['status' => 'success', 'data' => $kecamatan]);
    }

    public function getKelurahan(Request $request)
    {
        $kelurahan = RefKelurahan::where('kd_kecamatan', $request->kd_kecamatan)->orderBy('nm_kelurahan')->get();
        return response()->json(['status' => 'success', 'data' => $kelurahan]);
    }
}
