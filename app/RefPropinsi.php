<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefPropinsi extends Model
{
    protected $table = 'ref_propinsi';
    protected $primaryKey = 'kd_propinsi';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = ['kd_propinsi', 'nm_propinsi'];

    public function sppt()
    {
        return $this->hasMany(Sppt::class, 'kd_propinsi', 'kd_propinsi');
    }
}
