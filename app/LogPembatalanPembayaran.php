<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogPembatalanPembayaran extends Model
{
    protected $table = 'log_pembatalan_pembayaran';
    public $incrementing = false;
    public $timestamps = false;
    protected $guarded = [];
}
