<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefDati2 extends Model
{
    protected $table = 'ref_dati2';
    protected $primaryKey = 'kd_dati2';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = ['kd_propinsi', 'kd_dati2', 'nm_dati2'];

    public function sppt()
    {
        return $this->hasMany(Sppt::class, 'kd_dati2', 'kd_dati2');
    }
}
