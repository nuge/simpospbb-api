<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PospbbOptions extends Model
{
    protected $table = 'pospbb_options';
    protected $primaryKey = 'kode';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = [
    	'kode', 'nilai', 'ket', 'tipe', 'catatan', 'class'
    ];
}
