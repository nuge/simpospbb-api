<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefKelurahan extends Model
{
    protected $table = 'ref_kelurahan';
    protected $primaryKey = 'kd_kelurahan';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = [
    	'kd_propinsi', 'kd_dati2', 'kd_kecamatan', 'kd_kelurahan', 'kd_sektor', 'nm_kelurahan', 'no_kelurahan', 'kd_pos_kelurahan'
    ];

    public function sppt()
    {
    	return $this->hasMany(Sppt::class, 'kd_kelurahan', 'kd_kelurahan');
    }

    // public function pembayaran_sppt()
    // {
    //     return $this->hasMany(Pembayaran_sppt::class, 'kd_kelurahan', 'kd_kelurahan');
    // }
}
