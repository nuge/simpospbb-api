<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogPospbb extends Model
{
    protected $table = 'log_pospbb';
    protected $primaryKey = 'USERNAME';
    public $incrementing = false;
    public $timestamps = false;
    protected $dates = ['dt'];
    protected $fillable = [
    	'USERNAME', 
    	'DT', 
    	'AKTIVITAS', 
    	'IP', 
    	'NAMAHOSE', 
    	'NOP', 
    	'THN_PAJAK_SPPT'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class, 'username', 'username');
    }
}
