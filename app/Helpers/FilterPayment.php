<?php
if (!function_exists('FilterPayment')) {
    function FilterPayment() {
        $pospbb_options = \App\PospbbOptions::get();
        $end_time = '23:59:00';
        $end_day = 'Sun';
        $add_day = 2;

        foreach ($pospbb_options as $options) {
            if ($options->KODE == 1) {
                switch ($options->NILAI) {
                    case '6':
                        $end_day = 'Sat';
                        $add_day = 2;
                        break;
                    case '5':
                        $end_day = 'Fri';
                        $add_day = 3;
                        break;
                    case '4':
                        $end_day = 'Thu';
                        $add_day = 4;
                        break;
                    case '3':
                        $end_day = 'Wed';
                        $add_day = 5;
                        break;
                    case '2':
                        $end_day = 'Tue';
                        $add_day = 6;
                        break;
                    case '1':
                        $end_day = 'Mon';
                        $add_day = 7;
                        break;
                    default:
                        $end_day = 'Sun';
                        $add_day = 0;
                        break;
                }
            } elseif ($options->kode == 2) {
                $end_time = $options->nilai;
            }
        }
        $data = [
            'end_day' => $end_day,
            'add_day' => $add_day,
            'end_time' => $end_time
        ];
        return $data;
    }
}