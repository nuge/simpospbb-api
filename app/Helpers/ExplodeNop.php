<?php
if (!function_exists('ExplodeNOP')) {
    function ExplodeNOP($nop) {
        $getID = str_split($nop);
        $kd_propinsi = $getID[0] . $getID[1];
        $kd_dati2 = $getID[2] . $getID[3];
        $kd_kecamatan = $getID[4] . $getID[5] . $getID[6];
        $kd_kelurahan = $getID[7] . $getID[8] . $getID[9];
        $kd_blok = $getID[10] . $getID[11] . $getID[12];
        $no_urut = $getID[13] . $getID[14] . $getID[15] . $getID[16];
        $kd_jns_op = $getID[17];

        $thn_pajak_sppt = null;
        if (count($getID) == 22) {
            $thn_pajak_sppt = $getID[18] . $getID[19] . $getID[20] . $getID[21];
        }

        $data = [
            'kd_propinsi' => $kd_propinsi,
            'kd_dati2' => $kd_dati2,
            'kd_kecamatan' => $kd_kecamatan,
            'kd_kelurahan' => $kd_kelurahan,
            'kd_blok' => $kd_blok,
            'no_urut' => $no_urut,
            'kd_jns_op' => $kd_jns_op,
            'thn_pajak_sppt' => $thn_pajak_sppt
        ];
        return $data;
    }
}