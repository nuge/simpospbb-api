<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\DWComposite;

class DatObjekPajak extends Model
{
    use DWComposite;
    protected $table = 'dat_objek_pajak';
    protected $primaryKey = [
        'kd_propinsi',
        'kd_dati2',
        'kd_kecamatan',
        'kd_kelurahan',
        'kd_blok',
        'no_urut',
        'kd_jns_op',
        'subjek_pajak_id'
    ];
    public $incrementing = false;
    public $timestamps = false;

    public function dsp()
    {
        return $this->belongsTo(DatSubjekPajak::class, 'subjek_pajak_id', 'subjek_pajak_id');
    }

    public function scopeNop($query, $kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, $q)
    {
        if (!empty($q)) {
            return $query->where('kd_propinsi', $kd_propinsi)
                ->where('kd_dati2', $kd_dati2)
                ->where('kd_kecamatan', $kd_kecamatan)
                ->where('kd_kelurahan', $kd_kelurahan)
                ->where('kd_blok', $kd_blok)
                ->where('no_urut', $no_urut)
                ->where('kd_jns_op', $kd_jns_op)
                ->whereHas('dsp', function($query) {
                    $query->where('nm_wp', 'LIKE', '%' . $q . '%');
                });
        }
        return $query->where('kd_propinsi', $kd_propinsi)
            ->where('kd_dati2', $kd_dati2)
            ->where('kd_kecamatan', $kd_kecamatan)
            ->where('kd_kelurahan', $kd_kelurahan)
            ->where('kd_blok', $kd_blok)
            ->where('no_urut', $no_urut)
            ->where('kd_jns_op', $kd_jns_op);
    }
}
