<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefKecamatan extends Model
{
    protected $table = 'ref_kecamatan';
    protected $primaryKey = 'kd_kecamatan';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = [
    	'kd_propinsi', 'kd_dati2', 'kd_kecamatan', 'nm_kecamatan'
    ];

    public function sppt()
    {
    	return $this->hasMany(Sppt::class, 'kd_kecamatan', 'kd_kecamatan');
    }

    // public function pembayaran_sppt()
    // {
    //     return $this->hasMany(Pembayaran_sppt::class, 'kd_kecamatan', 'kd_kecamatan');
    // }
}
