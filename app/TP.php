<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\DwComposite;

class TP extends Model
{
    use DWComposite;
    protected $table = 'tempat_pembayaran';
    protected $primaryKey = ['kd_kanwil', 'kd_kppbb', 'kd_bank_tunggal', 'kd_bank_persepsi', 'kd_tp'];
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = [
    	'KD_KANWIL', 
    	'KD_KPPBB', 
    	'KD_BANK_TUNGGAL',
    	'KD_BANK_PERSEPSI',
    	'KD_TP',
    	'NM_TP',
    	'ALAMAT_TP',
    	'NO_REK_TP',
    	'AKTIF',
    	'KD_PROPINSI',
    	'KD_DATI2'
    ];

    protected $appends = ['splittp', 'kodetp'];

    public function getSplitTPAttribute()
    {
        return "{$this->kd_kanwil}{$this->kd_kppbb}{$this->kd_bank_tunggal}{$this->kd_bank_persepsi}{$this->kd_tp}";
    }
	
	public function getKodeTPAttribute()
    {
        return "{$this->kd_kanwil}.{$this->kd_kppbb}.{$this->kd_bank_tunggal}.{$this->kd_bank_persepsi}.{$this->kd_tp}";
    }
	
	public function scopePembayaranSppt($query, $kd_kanwil_bank, $kd_kppbb_bank, $kd_bank_tunggal, $kd_bank_persepsi, $kd_tp) {
		return $query
			->where('tempat_pembayaran.kd_kanwil', $kd_kanwil_bank)
			->where('tempat_pembayaran.kd_kppbb', $kd_kppbb_bank)
			->where('tempat_pembayaran.kd_bank_tunggal', $kd_bank_tunggal)
			->where('tempat_pembayaran.kd_bank_persepsi', $kd_bank_persepsi)
			->where('tempat_pembayaran.kd_tp', $kd_tp)
			->orderBy('tempat_pembayaran.kd_tp', 'ASC')
			->orderBy('tempat_pembayaran.kd_bank_persepsi', 'ASC')
			->orderBy('tempat_pembayaran.kd_bank_tunggal', 'ASC')
			->orderBy('tempat_pembayaran.kd_kppbb', 'ASC')
			->orderBy('tempat_pembayaran.kd_kanwil', 'ASC')
			->select('*');
	}
	
    public function user()
    {
        return $this->belongsTo(User::class, 'kd_tp', 'kd_tp');
    }
	
	public function pembayaran_sppt()
    {
        return $this->hasMany(Pembayaran_sppt::class, 'kd_tp', 'kd_tp');
    }
}
