<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\DwComposite;

class Sppt extends Model
{
    use DWComposite;
    protected $table = 'sppt';
    protected $primaryKey = ['kd_propinsi', 'kd_dati2', 'kd_kecamatan', 'kd_kelurahan', 'kd_blok', 'no_urut', 'kd_jns_op', 'thn_pajak_sppt'];
    protected $guarded = [];
    public $timestamps = false;
    protected $dates = ['tgl_jatuh_tempo_sppt', 'tgl_pembayaran_sppt'];

    public function getIdentitasAttribute()
    {
        return "{$this->kd_propinsi}{$this->kd_dati2}{$this->kd_kecamatan}{$this->kd_kelurahan}{$this->kd_blok}{$this->no_urut}{$this->kd_jns_op}{$this->thn_pajak_sppt}";
    }

    public function denda()
    {
		$jatuh_tempo = $this->tgl_jatuh_tempo_sppt;
		$dateNow = \Carbon\Carbon::now();
		$diff = $jatuh_tempo->diffInMonths($dateNow);
		$diff_month = $diff > 24 ? 24:$diff;
		return $this->pbb_yg_harus_dibayar_sppt * $diff_month * 0.02;
    }

    public function scopeInvoice($query, $kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, $thn_pajak_sppt = null, $status_pembayaran_sppt = 0)
    {
        return $query->where('kd_propinsi', $kd_propinsi)
            ->where('kd_dati2', $kd_dati2)
            ->where('kd_kecamatan', $kd_kecamatan)
            ->where('kd_kelurahan', $kd_kelurahan)
            ->where('kd_blok', $kd_blok)
            ->where('no_urut', $no_urut)
            ->where('kd_jns_op', $kd_jns_op)
            ->when($thn_pajak_sppt, function($q) use($thn_pajak_sppt) {
                $q->where('thn_pajak_sppt', $thn_pajak_sppt);
            })
            ->where('status_pembayaran_sppt', $status_pembayaran_sppt)
			->orderBy("thn_pajak_sppt", "DESC");
    }

    public function scopePembayaranSppt($query, $kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, $thn_pajak_sppt)
    {
        return $query->leftJoin('pembayaran_sppt', function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'pembayaran_sppt.kd_propinsi');
                $join->on('sppt.kd_dati2', '=', 'pembayaran_sppt.kd_dati2');
                $join->on('sppt.kd_kecamatan', '=', 'pembayaran_sppt.kd_kecamatan');
                $join->on('sppt.kd_kelurahan', '=', 'pembayaran_sppt.kd_kelurahan');
                $join->on('sppt.kd_blok', '=', 'pembayaran_sppt.kd_blok');
                $join->on('sppt.no_urut', '=', 'pembayaran_sppt.no_urut');
                $join->on('sppt.kd_jns_op', '=', 'pembayaran_sppt.kd_jns_op');
                $join->on('sppt.thn_pajak_sppt', '=', 'pembayaran_sppt.thn_pajak_sppt');
            })
            ->where('sppt.kd_propinsi', $kd_propinsi)
            ->where('sppt.kd_dati2', $kd_dati2)
            ->where('sppt.kd_kecamatan', $kd_kecamatan)
            ->where('sppt.kd_kelurahan', $kd_kelurahan)
            ->where('sppt.kd_blok', $kd_blok)
            ->where('sppt.no_urut', $no_urut)
            ->where('sppt.kd_jns_op', $kd_jns_op)
            ->where('sppt.thn_pajak_sppt', $thn_pajak_sppt)
			->orderBy("thn_pajak_sppt", "DESC")
            ->select(
                'sppt.kd_propinsi', 
                'sppt.kd_dati2', 
                'sppt.kd_kecamatan', 
                'sppt.kd_kelurahan', 
                'sppt.kd_blok', 
                'sppt.no_urut', 
                'sppt.kd_jns_op', 
                'sppt.thn_pajak_sppt', 
                'sppt.siklus_sppt', 
                'sppt.kd_kanwil_bank', 
                'sppt.kd_kppbb_bank', 
                'sppt.kd_bank_tunggal', 
                'sppt.kd_bank_persepsi', 
                'sppt.kd_tp', 
                'sppt.nm_wp_sppt', 
                'sppt.jln_wp_sppt', 
                'sppt.blok_kav_no_wp_sppt', 
                'sppt.rw_wp_sppt', 
                'sppt.rt_wp_sppt', 
                'sppt.kelurahan_wp_sppt', 
                'sppt.kota_wp_sppt', 
                'sppt.kd_pos_wp_sppt', 
                'sppt.npwp_sppt', 
                'sppt.no_persil_sppt', 
                'sppt.kd_kls_tanah', 
                'sppt.thn_awal_kls_tanah', 
                'sppt.kd_kls_bng', 
                'sppt.thn_awal_kls_bng', 
                'sppt.TGL_JATUH_TEMPO_SPPT', 
                'sppt.luas_bumi_sppt', 
                'sppt.luas_bng_sppt', 
                'sppt.njop_bumi_sppt', 
                'sppt.njop_bng_sppt', 
                'sppt.njop_sppt', 
                'sppt.njoptkp_sppt', 
                'sppt.njkp_sppt', 
                'sppt.pbb_terhutang_sppt', 
                'sppt.faktor_pengurang_sppt', 
                'sppt.pbb_yg_harus_dibayar_sppt', 
                'sppt.status_pembayaran_sppt', 
                'sppt.status_tagihan_sppt', 
                'sppt.status_cetak_sppt', 
                'sppt.tgl_terbit_sppt', 
                'sppt.tgl_cetak_sppt', 
                'sppt.nip_pencetak_sppt', 
                'pembayaran_sppt.pembayaran_sppt_ke', 
                'pembayaran_sppt.denda_sppt', 
                'pembayaran_sppt.jml_sppt_yg_dibayar', 
                'pembayaran_sppt.tgl_pembayaran_sppt', 
                'pembayaran_sppt.tgl_rekam_byr_sppt', 
                'pembayaran_sppt.nip_rekam_byr_sppt'
            );
    }

    public function ref_kecamatan()
    {
        return $this->belongsTo(RefKecamatan::class, 'kd_kecamatan', 'kd_kecamatan');
    }

    public function ref_kelurahan()
    {
        return $this->belongsTo(RefKelurahan::class, 'kd_kelurahan', 'kd_kelurahan');
    }

    public function ref_propinsi()
    {
        return $this->belongsTo(RefPropinsi::class, 'kd_propinsi', 'kd_propinsi');
    }

    public function ref_dati2()
    {
        return $this->belongsTo(RefDati2::class, 'kd_dati2', 'kd_dati2');
    }
}
