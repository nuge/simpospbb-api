<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = 'pegawai';
    protected $primaryKey = 'NIP';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = [
    	'NIP', 'NM_PEGAWAI', 'NIP_BARU'
    ];
}
