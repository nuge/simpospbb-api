<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\LogPospbb;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable;
    protected $guarded = [];
    // protected $fillable = [
    //     'ID',
    //     'NAME', 
    //     'EMAIL', 
    //     'PASSWORD', 
    //     'NIP', 
    //     'NIP_BARU', 
    //     'KD_KANWIL', 
    //     'KD_KPPBB',
    //     'KD_BANK_TUNGGAL',
    //     'KD_BANK_PERSEPSI',
    //     'KD_TP',
    //     'IS_AKTIF',
    //     'ALAMAT',
    //     'TELP',
    //     'USERNAME',
    //     'PHOTO',
    //     'USER_AGENT',
    //     'api_token'
    // ];

    // protected $hidden = [
    //     'password', 'remember_token',
    // ];
    protected $appends = ['status_label'];

    public function getStatusLabelAttribute()
    {
        if ($this->is_aktif == 0) {
            return '<span class="badge badge-secondary">Tidak Aktif</span>';
        }
        return '<span class="badge badge-success">Aktif</span>';
    }

    public function ref_kppbb()
    {
        return $this->belongsTo(Ref_kppbb::class, 'kd_kppbb', 'kd_kppbb');
    }

    public function ref_kanwil()
    {
        return $this->belongsTo(Ref_kanwil::class, 'kd_kanwil', 'kd_kanwil');
    }

    public function getSplitTPAttribute()
    {
        return "{$this->kd_kanwil}{$this->kd_kppbb}{$this->kd_bank_tunggal}{$this->kd_bank_persepsi}{$this->kd_tp}";
    }

    public function scopeAktif($query)
    {
        return $query->where('is_aktif', 1);
    }

    public function logAuth($username, $ip, $aktivitas)
    {
        $log_pospbb = LogPospbb::create([
            'USERNAME' => $username,
            'DT' => Carbon::now()->format('Y-m-d'),
            'AKTIVITAS' => $aktivitas,
            'IP' => $ip,
            'NAMAHOSE' => $ip,
            'NOP' => null,
            'THN_PAJAK_SPPT' => null
        ]);
        return $log_pospbb;
    }

    public function scopeTp($query)
    {
        return $query->leftJoin('tempat_pembayaran', function ($join) {
            $join->on('users.kd_kanwil', '=', 'tempat_pembayaran.kd_kanwil');
            $join->on('users.kd_kppbb', '=', 'tempat_pembayaran.kd_kppbb');
            $join->on('users.kd_bank_tunggal', '=', 'tempat_pembayaran.kd_bank_tunggal');
            $join->on('users.kd_bank_persepsi', '=', 'tempat_pembayaran.kd_bank_persepsi');
            $join->on('users.kd_tp', '=', 'tempat_pembayaran.kd_tp');
        });
    }

    public function scopePembayaran_sppt($query)
    {
        return $query->leftJoin('pembayaran_sppt', function ($join) {
            $join->on('pembayaran_sppt.kd_kanwil_bank', '=', 'users.kd_kanwil');
            $join->on('pembayaran_sppt.kd_kppbb_bank', '=', 'users.kd_kppbb');
            $join->on('pembayaran_sppt.kd_bank_tunggal', '=', 'users.kd_bank_tunggal');
            $join->on('pembayaran_sppt.kd_bank_persepsi', '=', 'users.kd_bank_persepsi');
            $join->on('pembayaran_sppt.kd_tp', '=', 'users.kd_tp');
        });
    }

    public function pegawai()
    {
        return $this->hasOne(Pegawai::class, 'nip', 'nip');
    }

    public function log_pospbb()
    {
        return $this->hasMany(Log_pospbb::class, 'username', 'username');
    }
	
	/*
    public function role()
    {
        return $this->belongsToMany(Role::class, 'user_roles', 'user_id', 'id_group');
    }
	*/
	///
	public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
	
    public function permissions()
    {
        return $this->hasManyThrough(Permission::class, Role::class);
    }
	
	public function isAdmin()
    {
       if ($this->roles->contains('name', 'admin')) {
            return true;
        }

        return false;
    }
	
	public function hasRole($name, $requireAll = false)
    {
        if (is_array($name)) {
            foreach ($name as $roleName) {
                $hasRole = $this->hasRole($roleName);
                if ($hasRole && !$requireAll) {
                    return true;
                } elseif (!$hasRole && $requireAll) {
                    return false;
                }
            }

            // If we've made it this far and $requireAll is FALSE, then NONE of the roles were found
            // If we've made it this far and $requireAll is TRUE, then ALL of the roles were found.
            // Return the value of $requireAll;
            return $requireAll;
        } else {
			if (is_string($name)) {
				return $this->roles->contains('name', $name);
			}
			return !! $name->intersect($this->roles)->count();
        }
		return false;
    }
	
	public function assignRole($role)
    {
        if (is_string($role)) {
            $role = Role::where('name', $role)->first();
        }

        return $this->roles()->attach($role);
    }

    public function revokeRole($role)
    {
        if (is_string($role)) {
            $role = Role::where('name', $role)->first();
        }
        return $this->roles()->detach($role);
    }
	
	 /**
     * Alias to eloquent many-to-many relation's attach() method.
     *
     * @param mixed $role
     */
	 
	public function attachRole($role)
    {
		if(is_object($role)) {
            $role = $role->getKey();
        }

        if(is_array($role)) {
            $role = $role['id'];
        }

        $this->roles()->attach($role);
		
    }
	 /**
     * Alias to eloquent many-to-many relation's detach() method.
     *
     * @param mixed $role
     */
    public function detachRole($role)
    {
        if (is_object($role)) {
            $role = $role->getKey();
        }

        if (is_array($role)) {
            $role = $role['id'];
        }

        $this->roles()->detach($role);
    }
	
	 /**
     * Attach multiple roles to a user
     *
     * @param mixed $roles
     */
    public function attachRoles($roles)
    {
        foreach ($roles as $role) {
            $this->attachRole($role);
        }
    }
	
	 /**
     * Detach multiple roles from a user
     *
     * @param mixed $roles
     */
    public function detachRoles($roles=null)
    {
        if (!$roles) $roles = $this->roles()->get();
        
        foreach ($roles as $role) {
            $this->detachRole($role);
        }
    }
	
	/**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
