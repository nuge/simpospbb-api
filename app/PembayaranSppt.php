<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PembayaranSppt extends Model
{
    protected $table = 'pembayaran_sppt';
    protected $primaryKey = 'nip_rekam_byr_sppt';
    public $incrementing = false;
    public $timestamps = false;
    protected $guarded = [];
    protected $dates = ['tgl_jatuh_tempo_sppt','tgl_pembayaran_sppt'];
    
    public function getIdentitasAttribute()
    {
        return "{$this->kd_propinsi}{$this->kd_dati2}{$this->kd_kecamatan}{$this->kd_kelurahan}{$this->kd_blok}{$this->no_urut}{$this->kd_jns_op}{$this->thn_pajak_sppt}";
    }
	
	public function scopeBill($query, $kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op)
    {
        return $query->join('sppt', function ($join) {
                $join->on('sppt.kd_propinsi', '=', 'pembayaran_sppt.kd_propinsi');
                $join->on('sppt.kd_dati2', '=', 'pembayaran_sppt.kd_dati2');
                $join->on('sppt.kd_kecamatan', '=', 'pembayaran_sppt.kd_kecamatan');
                $join->on('sppt.kd_kelurahan', '=', 'pembayaran_sppt.kd_kelurahan');
                $join->on('sppt.kd_blok', '=', 'pembayaran_sppt.kd_blok');
                $join->on('sppt.no_urut', '=', 'pembayaran_sppt.no_urut');
                $join->on('sppt.kd_jns_op', '=', 'pembayaran_sppt.kd_jns_op');
                $join->on('sppt.thn_pajak_sppt', '=', 'pembayaran_sppt.thn_pajak_sppt');
            })
            ->where('sppt.kd_propinsi', $kd_propinsi)
            ->where('sppt.kd_dati2', $kd_dati2)
            ->where('sppt.kd_kecamatan', $kd_kecamatan)
            ->where('sppt.kd_kelurahan', $kd_kelurahan)
            ->where('sppt.kd_blok', $kd_blok)
            ->where('sppt.no_urut', $no_urut)
            ->where('sppt.kd_jns_op', $kd_jns_op)
            //->where('sppt.thn_pajak_sppt', $thn_pajak_sppt)
            ->select(
                'sppt.kd_propinsi', 
                'sppt.kd_dati2', 
                'sppt.kd_kecamatan', 
                'sppt.kd_kelurahan', 
                'sppt.kd_blok', 
                'sppt.no_urut', 
                'sppt.kd_jns_op', 
                'sppt.thn_pajak_sppt', 
                'sppt.siklus_sppt', 
                'sppt.kd_kanwil_bank', 
                'sppt.kd_kppbb_bank', 
                'sppt.kd_bank_tunggal', 
                'sppt.kd_bank_persepsi', 
                'sppt.kd_tp', 
                'sppt.nm_wp_sppt', 
                'sppt.jln_wp_sppt', 
                'sppt.blok_kav_no_wp_sppt', 
                'sppt.rw_wp_sppt',
                'sppt.rt_wp_sppt', 
                'sppt.kelurahan_wp_sppt', 
                'sppt.kota_wp_sppt', 
                'sppt.kd_pos_wp_sppt', 
                'sppt.npwp_sppt', 
                'sppt.no_persil_sppt', 
                'sppt.kd_kls_tanah', 
                'sppt.thn_awal_kls_tanah', 
                'sppt.kd_kls_bng', 
                'sppt.thn_awal_kls_bng', 
                'sppt.tgl_jatuh_tempo_sppt', 
                'sppt.luas_bumi_sppt', 
                'sppt.luas_bng_sppt', 
                'sppt.njop_bumi_sppt', 
                'sppt.njop_bng_sppt', 
                'sppt.njop_sppt', 
                'sppt.njoptkp_sppt', 
                'sppt.njkp_sppt', 
                'sppt.pbb_terhutang_sppt', 
                'sppt.faktor_pengurang_sppt', 
                'sppt.pbb_yg_harus_dibayar_sppt', 
                'sppt.status_pembayaran_sppt', 
                'sppt.status_tagihan_sppt', 
                'sppt.status_cetak_sppt', 
                'sppt.tgl_terbit_sppt', 
                'sppt.tgl_cetak_sppt', 
                'sppt.nip_pencetak_sppt', 
                'pembayaran_sppt.pembayaran_sppt_ke', 
                'pembayaran_sppt.denda_sppt', 
                'pembayaran_sppt.jml_sppt_yg_dibayar', 
                'pembayaran_sppt.tgl_pembayaran_sppt', 
                'pembayaran_sppt.tgl_rekam_byr_sppt', 
                'pembayaran_sppt.nip_rekam_byr_sppt'
            );
    }

    public function scopeBill3($query, $kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $no_urut, $kd_jns_op, $thn_pajak_sppt)
    {
		if (is_array($thn_pajak_sppt)) {
			return  $query->join('sppt', function ($join) {
					$join->on('sppt.kd_propinsi', '=', 'pembayaran_sppt.kd_propinsi');
					$join->on('sppt.kd_dati2', '=', 'pembayaran_sppt.kd_dati2');
					$join->on('sppt.kd_kecamatan', '=', 'pembayaran_sppt.kd_kecamatan');
					$join->on('sppt.kd_kelurahan', '=', 'pembayaran_sppt.kd_kelurahan');
					$join->on('sppt.kd_blok', '=', 'pembayaran_sppt.kd_blok');
					$join->on('sppt.no_urut', '=', 'pembayaran_sppt.no_urut');
					$join->on('sppt.kd_jns_op', '=', 'pembayaran_sppt.kd_jns_op');
					$join->on('sppt.thn_pajak_sppt', '=', 'pembayaran_sppt.thn_pajak_sppt');
				})->join('ref_kecamatan', function($join) {
					$join->on('ref_kecamatan.kd_propinsi', '=', 'pembayaran_sppt.kd_propinsi');
					$join->on('ref_kecamatan.kd_dati2', '=', 'pembayaran_sppt.kd_dati2');
					$join->on('ref_kecamatan.kd_kecamatan', '=', 'pembayaran_sppt.kd_kecamatan');
				})->join('ref_kelurahan', function($join) {
					$join->on('ref_kelurahan.kd_propinsi', '=', 'pembayaran_sppt.kd_propinsi');
					$join->on('ref_kelurahan.kd_dati2', '=', 'pembayaran_sppt.kd_dati2');
					$join->on('ref_kelurahan.kd_kecamatan', '=', 'pembayaran_sppt.kd_kecamatan');
					$join->on('ref_kelurahan.kd_kelurahan', '=', 'pembayaran_sppt.kd_kelurahan');
				})->join('tempat_pembayaran', function($join) {
					$join->on('tempat_pembayaran.kd_kanwil', '=', 'pembayaran_sppt.kd_kanwil_bank');
					$join->on('tempat_pembayaran.kd_kppbb', '=', 'pembayaran_sppt.kd_kppbb_bank');
					$join->on('tempat_pembayaran.kd_bank_tunggal', '=', 'pembayaran_sppt.kd_bank_tunggal');
					$join->on('tempat_pembayaran.kd_bank_persepsi', '=', 'pembayaran_sppt.kd_bank_persepsi');
					$join->on('tempat_pembayaran.kd_tp', '=', 'pembayaran_sppt.kd_tp');
				}) 
				->where('sppt.kd_propinsi', $kd_propinsi)
				->where('sppt.kd_dati2', $kd_dati2)
				->where('sppt.kd_kecamatan', $kd_kecamatan)
				->where('sppt.kd_kelurahan', $kd_kelurahan)
				->where('sppt.kd_blok', $kd_blok)
				->where('sppt.no_urut', $no_urut)
				->where('sppt.kd_jns_op', $kd_jns_op)
				->whereIn('sppt.thn_pajak_sppt', $thn_pajak_sppt)
				
				->select('sppt.kd_propinsi', 'sppt.kd_dati2', 
					'sppt.kd_kecamatan', 'sppt.kd_kelurahan', 
					'sppt.kd_blok', 'sppt.no_urut', 
					'sppt.kd_jns_op', 'sppt.thn_pajak_sppt', 
					'ref_kecamatan.nm_kecamatan', 'ref_kelurahan.nm_kelurahan', 
					'sppt.siklus_sppt', 'sppt.kd_kanwil_bank', 
					'sppt.kd_kppbb_bank', 'sppt.kd_bank_tunggal', 
					'sppt.kd_bank_persepsi', 'sppt.kd_tp', 
					'tempat_pembayaran.nm_tp', 'tempat_pembayaran.alamat_tp',
					'tempat_pembayaran.no_rek_tp', 
					'sppt.nm_wp_sppt', 'sppt.jln_wp_sppt', 
					'sppt.blok_kav_no_wp_sppt', 'sppt.rw_wp_sppt', 
					'sppt.rt_wp_sppt', 'sppt.kelurahan_wp_sppt', 
					'sppt.kota_wp_sppt', 'sppt.kd_pos_wp_sppt', 
					'sppt.npwp_sppt', 'sppt.no_persil_sppt', 
					'sppt.kd_kls_tanah', 'sppt.thn_awal_kls_tanah', 
					'sppt.kd_kls_bng', 'sppt.thn_awal_kls_bng', 
					'sppt.tgl_jatuh_tempo_sppt', 'sppt.luas_bumi_sppt', 
					'sppt.luas_bng_sppt', 'sppt.njop_bumi_sppt', 
					'sppt.njop_bng_sppt', 'sppt.njop_sppt',
					'sppt.njoptkp_sppt', 'sppt.njkp_sppt', 
					'sppt.pbb_terhutang_sppt', 'sppt.faktor_pengurang_sppt', 
					'sppt.pbb_yg_harus_dibayar_sppt', 'sppt.status_pembayaran_sppt', 
					'sppt.status_tagihan_sppt', 'sppt.status_cetak_sppt', 
					'sppt.tgl_terbit_sppt', 'sppt.tgl_cetak_sppt', 
					'sppt.nip_pencetak_sppt', 'pembayaran_sppt.pembayaran_sppt_ke', 
					'pembayaran_sppt.denda_sppt', 'pembayaran_sppt.jml_sppt_yg_dibayar', 
					'pembayaran_sppt.tgl_pembayaran_sppt', 
					'pembayaran_sppt.tgl_rekam_byr_sppt', 'pembayaran_sppt.nip_rekam_byr_sppt');
		} else {
			return  $query->join('sppt', function ($join) {
					$join->on('sppt.kd_propinsi', '=', 'pembayaran_sppt.kd_propinsi');
					$join->on('sppt.kd_dati2', '=', 'pembayaran_sppt.kd_dati2');
					$join->on('sppt.kd_kecamatan', '=', 'pembayaran_sppt.kd_kecamatan');
					$join->on('sppt.kd_kelurahan', '=', 'pembayaran_sppt.kd_kelurahan');
					$join->on('sppt.kd_blok', '=', 'pembayaran_sppt.kd_blok');
					$join->on('sppt.no_urut', '=', 'pembayaran_sppt.no_urut');
					$join->on('sppt.kd_jns_op', '=', 'pembayaran_sppt.kd_jns_op');
					$join->on('sppt.thn_pajak_sppt', '=', 'pembayaran_sppt.thn_pajak_sppt');
				})->join('ref_kecamatan', function($join) {
					$join->on('ref_kecamatan.kd_propinsi', '=', 'pembayaran_sppt.kd_propinsi');
					$join->on('ref_kecamatan.kd_dati2', '=', 'pembayaran_sppt.kd_dati2');
					$join->on('ref_kecamatan.kd_kecamatan', '=', 'pembayaran_sppt.kd_kecamatan');
				})->join('ref_kelurahan', function($join) {
					$join->on('ref_kelurahan.kd_propinsi', '=', 'pembayaran_sppt.kd_propinsi');
					$join->on('ref_kelurahan.kd_dati2', '=', 'pembayaran_sppt.kd_dati2');
					$join->on('ref_kelurahan.kd_kecamatan', '=', 'pembayaran_sppt.kd_kecamatan');
					$join->on('ref_kelurahan.kd_kelurahan', '=', 'pembayaran_sppt.kd_kelurahan');
				})->join('tempat_pembayaran', function($join) {
					$join->on('tempat_pembayaran.kd_kanwil', '=', 'pembayaran_sppt.kd_kanwil_bank');
					$join->on('tempat_pembayaran.kd_kppbb', '=', 'pembayaran_sppt.kd_kppbb_bank');
					$join->on('tempat_pembayaran.kd_bank_tunggal', '=', 'pembayaran_sppt.kd_bank_tunggal');
					$join->on('tempat_pembayaran.kd_bank_persepsi', '=', 'pembayaran_sppt.kd_bank_persepsi');
					$join->on('tempat_pembayaran.kd_tp', '=', 'pembayaran_sppt.kd_tp');
				}) 
				->where('sppt.kd_propinsi', $kd_propinsi)
				->where('sppt.kd_dati2', $kd_dati2)
				->where('sppt.kd_kecamatan', $kd_kecamatan)
				->where('sppt.kd_kelurahan', $kd_kelurahan)
				->where('sppt.kd_blok', $kd_blok)
				->where('sppt.no_urut', $no_urut)
				->where('sppt.kd_jns_op', $kd_jns_op)
				->where('sppt.thn_pajak_sppt', $thn_pajak_sppt)
				
				->select('sppt.kd_propinsi', 'sppt.kd_dati2', 
					'sppt.kd_kecamatan', 'sppt.kd_kelurahan', 
					'sppt.kd_blok', 'sppt.no_urut', 
					'sppt.kd_jns_op', 'sppt.thn_pajak_sppt', 
					'ref_kecamatan.nm_kecamatan', 'ref_kelurahan.nm_kelurahan', 
					'sppt.siklus_sppt', 'sppt.kd_kanwil_bank', 
					'sppt.kd_kppbb_bank', 'sppt.kd_bank_tunggal', 
					'sppt.kd_bank_persepsi', 'sppt.kd_tp', 
					'tempat_pembayaran.nm_tp', 'tempat_pembayaran.alamat_tp',
					'tempat_pembayaran.no_rek_tp', 
					'sppt.nm_wp_sppt', 'sppt.jln_wp_sppt', 
					'sppt.blok_kav_no_wp_sppt', 'sppt.rw_wp_sppt', 
					'sppt.rt_wp_sppt', 'sppt.kelurahan_wp_sppt', 
					'sppt.kota_wp_sppt', 'sppt.kd_pos_wp_sppt', 
					'sppt.npwp_sppt', 'sppt.no_persil_sppt', 
					'sppt.kd_kls_tanah', 'sppt.thn_awal_kls_tanah', 
					'sppt.kd_kls_bng', 'sppt.thn_awal_kls_bng', 
					'sppt.tgl_jatuh_tempo_sppt', 'sppt.luas_bumi_sppt', 
					'sppt.luas_bng_sppt', 'sppt.njop_bumi_sppt', 
					'sppt.njop_bng_sppt', 'sppt.njop_sppt',
					'sppt.njoptkp_sppt', 'sppt.njkp_sppt', 
					'sppt.pbb_terhutang_sppt', 'sppt.faktor_pengurang_sppt', 
					'sppt.pbb_yg_harus_dibayar_sppt', 'sppt.status_pembayaran_sppt', 
					'sppt.status_tagihan_sppt', 'sppt.status_cetak_sppt', 
					'sppt.tgl_terbit_sppt', 'sppt.tgl_cetak_sppt', 
					'sppt.nip_pencetak_sppt', 'pembayaran_sppt.pembayaran_sppt_ke', 
					'pembayaran_sppt.denda_sppt', 'pembayaran_sppt.jml_sppt_yg_dibayar', 
					'pembayaran_sppt.tgl_pembayaran_sppt', 
					'pembayaran_sppt.tgl_rekam_byr_sppt', 'pembayaran_sppt.nip_rekam_byr_sppt');

		}
    }

    public function kecamatan()
    {
        return $this->belongsTo(RefKecamatan::class, 'kd_kecamatan', 'kd_kecamatan');
    }

    public function kelurahan()
    {
        return $this->belongsTo(RefKelurahan::class, 'kd_kelurahan', 'kd_kelurahan');
	}
	
	public function pegawai()
	{
		return $this->belongsTo(Pegawai::class, 'nip_rekam_byr_sppt', 'nip');
	}
}
