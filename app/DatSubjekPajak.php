<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatSubjekPajak extends Model
{
    protected $table = 'dat_subjek_pajak';
    protected $primaryKey = 'subjek_pajak_id';
    public $incrementing = false;
    public $timestamps = false;

    public function dop()
    {
        return $this->hasOne(DatObjekPajak::class, 'subjek_pajak_id', 'subjek_pajak_id');
    }

    public function scopeWhereLike($query, $nm_wp, $alamat, $q)
    {
        if (!empty($nm_wp) && !empty($alamat)) {
            if (!empty($q)) {
                return $query->where('nm_wp', 'LIKE', '%' . strtoupper($nm_wp) . '%')
                    ->where('jalan_wp', 'LIKE', '%' . strtoupper($alamat) . '%')
                    ->where('nm_wp', 'LIKE', '%' . strtoupper($q) . '%');
            }
            return $query->where('nm_wp', 'LIKE', '%' . strtoupper($nm_wp) . '%')
                ->where('jalan_wp', 'LIKE', '%' . strtoupper($alamat) . '%');
        } elseif (!empty($nm_wp)) {
            if (!empty($q)) {
                return $query->where('nm_wp', 'LIKE', '%' . strtoupper($q) . '%');
            }
            return $query->where('nm_wp', 'LIKE', '%' . strtoupper($nm_wp) . '%');
        } 

        if (!empty($q)) {
            return $query->where('jalan_wp', 'LIKE', '%' . strtoupper($alamat) . '%')
                ->where('nm_wp', 'LIKE', '%' . strtoupper($q) . '%');
        }
		
        return $query->where('jalan_wp', 'LIKE', '%' . strtoupper($alamat) . '%');
    }
}
