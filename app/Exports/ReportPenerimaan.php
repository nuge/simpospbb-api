<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ReportPenerimaan implements FromView, ShouldAutoSize
{
    use Exportable;
    
    protected $penerimaan;
    protected $headerYear;
    public function __construct($penerimaan, $headerYear)
    {
        $this->penerimaan = $penerimaan;
        $this->headerYear = $headerYear;
    }

    public function view(): View
    {   
        return view('exports.report_penerimaan', [
            'penerimaan' => $this->penerimaan,
            'headerYear' => $this->headerYear
        ]);
    }
}
