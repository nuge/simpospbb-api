<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ReportTunggakan implements FromView, ShouldAutoSize
{
    use Exportable;
    
    protected $sppt;
    protected $start;
    protected $end;

    public function __construct($sppt, $start, $end)
    {
        $this->sppt = $sppt;
        $this->start = $start;
        $this->end = $end;
    }

    public function view(): View
    {   
        return view('exports.report_tunggakan', [
            'sppt' => $this->sppt,
            'start' => $this->start,
            'end' => $this->end,
        ]);
    }
}
