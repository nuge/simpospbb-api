<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ReportPenerimaanBerjalan implements FromView, ShouldAutoSize
{
    use Exportable;
    
    protected $penerimaan;
    public function __construct($penerimaan)
    {
        $this->penerimaan = $penerimaan;
    }

    public function view(): View
    {   
        return view('exports.report_penerimaan_berjalan', [
            'penerimaan' => $this->penerimaan
        ]);
    }
}
