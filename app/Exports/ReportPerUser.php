<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ReportPerUser implements FromView, ShouldAutoSize, WithEvents
{
    use Exportable;

    protected $pembayaran;
    protected $start;
    protected $end;
    
    public function __construct($pembayaran, $start, $end)
    {
        $this->pembayaran = $pembayaran;
        $this->start = $start;
        $this->end = $end;
    }

    public function view(): View
    {
        $total_stts = $this->pembayaran->sum(function($item) {
			return $item->jumlah_stts;
		});
		$total_pbb = $this->pembayaran->sum(function($item) {
			return $item->pbb_pokok;
		});
		$total_denda = $this->pembayaran->sum(function($item) {
			return $item->jumlah_denda;
        });
        
        return view('exports.report_per_user', [
            'pembayaran' => $this->pembayaran,
            'total_stts' => $total_stts,
            'total_pbb' => $total_pbb,
            'total_denda' => $total_denda,
            'start' => $this->start,
            'end' => $this->end
        ]);
    }
}
