<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ReportWilayah implements FromView, ShouldAutoSize
{
    use Exportable;
    
    protected $pembayaran;
    protected $stts;
    protected $pbb;
    protected $denda;
    protected $start;
    protected $end;

    public function __construct($pembayaran, $stts, $pbb, $denda, $start, $end)
    {
        $this->pembayaran = $pembayaran;
        $this->stts = $stts;
        $this->pbb = $pbb;
        $this->denda = $denda;
        $this->start = $start;
        $this->end = $end;
    }

    public function view(): View
    {   
        return view('exports.report_wilayah', [
            'pembayaran' => $this->pembayaran,
            'stts' => $this->stts,
            'pbb' => $this->pbb,
            'denda' => $this->denda,
            'start' => $this->start,
            'end' => $this->end,
        ]);
    }
}
