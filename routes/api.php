<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Auth\LoginController@login');
Route::post('captcha', 'Auth\LoginController@getCaptchaImage');

Route::group(['middleware' => 'auth:api'], function() {
    Route::post('logout', 'Auth\LoginController@logout');
    Route::get('summary', 'HomeController@summaryDashboard');
    Route::get('summary-chart', 'HomeController@summaryChart');

    Route::get('objek-pajak/search', 'ObjekPajakController@search');
    Route::get('rekam-jejak', 'RekamJejakController@index');
    Route::get('rekam-jejak/pdf', 'RekamJejakController@rekamJejakPdf');
    Route::get('pembayaran/find', 'PembayaranController@findData');
    Route::post('pembayaran', 'PembayaranController@simpanPembayaran');
    Route::get('pembayaran/kolektif', 'PembayaranController@kolektif');
    Route::get('pembayaran/batal', 'PembayaranController@pembatalanPembayaran');
    Route::post('pembayaran/batal', 'PembayaranController@prosesPembatalan');
    Route::post('pembayaran/kurang-bayar/find', 'PembayaranController@findDataKurangBayar');
    Route::post('pembayaran/kurang-bayar', 'PembayaranController@storePembayaranKurangBayar');

    Route::post('bukti-bayar', 'PembayaranController@cetakBuktiPembayaran');

    Route::get('kecamatan', 'RegionalController@getKecamatan');
    Route::get('kelurahan', 'RegionalController@getKelurahan');
    Route::get('tp', 'ReportController@getTP');
    Route::get('/pegawai', 'ReportController@getPegawai');

    Route::group(['prefix' => 'laporan'], function() {
        Route::get('/wilayah', 'ReportController@reportPerWilayah');
        Route::get('/wilayah/pdf', 'ReportController@reportPerWilayahPDF');
        Route::get('/wilayah/excel', 'ReportController@reportPerWilayahExcel');
        
        Route::get('/tp', 'ReportController@reportTP');
        Route::get('/tp/pdf', 'ReportController@reportTpPDF');
        Route::get('/tp/excel', 'ReportController@reportPerTpExcel');

        Route::get('/tunggakan', 'ReportController@reportTunggakan');
        Route::get('/tunggakan/pdf', 'ReportController@reportTunggakanPdf');
        Route::get('/tunggakan/excel', 'ReportController@reportTunggakannExcel');
        
        Route::get('/user', 'ReportController@reportPerUser');
        Route::get('/user/pdf', 'ReportController@reportPerUserPDF');
        Route::get('/user/excel', 'ReportController@reportPerUserExcel');

        Route::get('/penerimaan', 'ReportController@reportPenerimaan');
        Route::get('/penerimaan/pdf', 'ReportController@reportPenerimaanPdf');
        Route::get('/penerimaan/excel', 'ReportController@reportPenerimaanExcel');

        Route::get('/penerimaan-berjalan', 'ReportController@reportPenerimaanBerjalan');
        Route::get('/penerimaan-berjalan/pdf', 'ReportController@reportPenerimaanBerjalanPdf');
        Route::get('/penerimaan-berjalan/excel', 'ReportController@reportPenerimaanBerjalanExcel');
    });

    Route::group(['prefix' => 'user'], function() {
        Route::get('/', 'UserController@index');
        Route::post('/', 'UserController@store');
        Route::put('/status/{id}', 'UserController@changeStatus');
    });

    Route::group(['prefix' => 'role'], function() {
        Route::get('/', 'RoleController@index');
        Route::post('/', 'RoleController@store');
        Route::put('/{id}', 'RoleController@update');
        Route::delete('/{id}', 'RoleController@destroy');
    });
});
Route::get('/report/pdf/{filename}', 'ReportController@getPdfFile')->name('report.pdf');